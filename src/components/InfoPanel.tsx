import { Box } from "@chakra-ui/react";
import { InfoIcon } from "@chakra-ui/icons";
import React, { FC } from "react";
import { theme } from "../styles/ChakraTheme";

export interface InfoPanelProps {
    readonly text: React.ReactNode | string;
}

export const InfoPanel: FC<InfoPanelProps> = ({ text }) => {
    return (
        <Box
            bg={theme.colors.brand.green}
            display="flex"
            alignItems="center"
            p="1.2rem"
            borderRadius="0.8rem"
        >
            <InfoIcon h="1.4rem" w="1.4rem" color="white" />

            <Box ml="1rem" lineHeight="2rem" fontSize="1rem" color="gray.600">
                {text}
            </Box>
        </Box>
    );
};
