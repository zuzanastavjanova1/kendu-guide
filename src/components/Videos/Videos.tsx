export {};
// import React, { FC } from "react";
// import {
//     Box,
//     Heading,
//     Text,
//     Tag,
//     HStack,
//     Button,
//     IconButton,
//     Link,
//     Tooltip,
//     Wrap,
//     Flex,
//     Spacer,
//     Badge,
// } from "@chakra-ui/react";
// import Slider from "react-slick";
// import { AiOutlineStar, AiFillStar } from "react-icons/ai";
// import { getButtons, getIcons } from "./getIcons";
// import { IVideos } from "../../services/useVideos";
// import { useSelectUniqueIds } from "../../hooks/useSelectUniqueIds";
// import { useFilteredCollection } from "../../services/data/useFilteredCollection";
// import { useAuth } from "../../../context/AuthUserContext";

// interface IVideosProps {
//     readonly title: string;
//     readonly nameCollection: string;
//     readonly filteredSection: string;
// }

// export const Videos: FC<IVideosProps> = ({
//     title,
//     nameCollection,
//     filteredSection,
// }) => {
//     const { data: videosData, isLoading } = useFilteredCollection({
//         nameCollection,
//         filteredSection,
//     });
//     const { userQuestions, userId } = useAuth();

//     const useSavedVideosQuestions = userQuestions?.map((i: string) => i) ?? [];

//     if (isLoading) {
//         return <div>Loading...</div>;
//     }

//     const settings = {
//         dots: true,
//         infinite: true,
//         speed: 500,
//         slidesToScroll: 1,
//         draggable: true,
//         slidesToShow: 4,
//         centerPadding: "40px",
//     };

//     return (
//         <Box p="10">
//             <Flex mb="6">
//                 <Box display="flex">
//                     <Heading as="h3" size="sm">
//                         {title}
//                     </Heading>
//                     <Badge colorScheme="purple" ml="6">
//                         Recommended
//                     </Badge>
//                 </Box>
//                 <Spacer />
//                 <Text fontSize="14" fontWeight="medium" color="brand.lila">
//                     {videosData?.length} Videos
//                 </Text>
//             </Flex>

//             <Slider {...settings}>
//                 {videosData?.map((data: any) => {
//                     const selectedItem = useSavedVideosQuestions.includes(
//                         data.id
//                     );
//                     const tooltipLabel = (
//                         <Box p="4">
//                             <Text fontWeight="semibold" fontSize="md">
//                                 {data.name}
//                             </Text>
//                             <Text>{data.author}</Text>
//                             <Text mt="4">{data.description}</Text>
//                         </Box>
//                     );
//                     // eslint-disable-next-line @typescript-eslint/ban-ts-comment
//                     // @ts-ignore
//                     const tags = videosData?.tags?.map((tag: string) => (
//                         <HStack spacing={2} key={tag}>
//                             <Tag size="sm">{tag}</Tag>
//                         </HStack>
//                     ));

//                     return (
//                         <Box p="4" key={data.id}>
//                             <Tooltip
//                                 hasArrow
//                                 label={tooltipLabel}
//                                 bg="brand.yellow"
//                                 color="black"
//                                 placement="right"
//                                 key={data.id}
//                             >
//                                 <Box
//                                     key={data.id}
//                                     bg="white"
//                                     p="4"
//                                     borderRadius={6}
//                                     boxShadow="0 3px 10px rgb(0 0 0 / 0.1)"
//                                 >
//                                     <Box display="flex" justifyContent="end">
//                                         <IconButton
//                                             as={
//                                                 selectedItem
//                                                     ? AiFillStar
//                                                     : AiOutlineStar
//                                             }
//                                             aria-label="start"
//                                             size="xs"
//                                             bg="white"
//                                             // onClick={() =>
//                                             //     handleSaveQuestion(data)
//                                             // }
//                                             _hover={{
//                                                 cursor: "pointer",
//                                             }}
//                                         />
//                                     </Box>
//                                     <Link href={data.link} isExternal>
//                                         <Heading
//                                             as="h6"
//                                             fontSize="md"
//                                             mb="2"
//                                             noOfLines={1}
//                                         >
//                                             {data.name}
//                                         </Heading>
//                                     </Link>
//                                     <Text color="grey">
//                                         {data.author ?? ""}
//                                     </Text>

//                                     <Text mt="2" noOfLines={2} fontSize="sm">
//                                         {data.description}
//                                     </Text>
//                                     <Wrap spacing="2" mt="4">
//                                         {tags}
//                                     </Wrap>
//                                     <Wrap spacing="2" mt="4">
//                                         {getButtons({ data }).map((i) => (
//                                             <Link
//                                                 href={i.link}
//                                                 isExternal
//                                                 key={i.label}
//                                             >
//                                                 <Button
//                                                     variant="link"
//                                                     size="xs"
//                                                     color="brand.lila"
//                                                 >
//                                                     {i.label}
//                                                 </Button>
//                                             </Link>
//                                         ))}
//                                     </Wrap>
//                                     <Box display="flex" justifyContent="end">
//                                         <Wrap spacing="4" mt="2">
//                                             {getIcons({ data }).map((i) => (
//                                                 <Link
//                                                     href={i.link}
//                                                     isExternal
//                                                     key={i.label}
//                                                 >
//                                                     <IconButton
//                                                         as={i.icon}
//                                                         size="xs"
//                                                         aria-label={i.label}
//                                                         bg="white"
//                                                         _hover={{
//                                                             cursor: "pointer",
//                                                         }}
//                                                     />
//                                                 </Link>
//                                             ))}
//                                         </Wrap>
//                                     </Box>
//                                 </Box>
//                             </Tooltip>
//                         </Box>
//                     );
//                 })}
//             </Slider>
//         </Box>
//     );
// };
