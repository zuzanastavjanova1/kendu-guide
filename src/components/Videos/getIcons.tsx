export {};
// import { AiFillGithub } from "react-icons/ai";
// import { FiInstagram } from "react-icons/fi";
// import { MdFacebook } from "react-icons/md";
// import { BsTwitter } from "react-icons/bs";

// // interface IVideos {
// //     readonly author: string;
// //     readonly blog: string | null;
// //     readonly courses: string | null;
// //     readonly description: string;
// //     readonly facebook: string | null;
// //     readonly github: string | null;
// //     readonly id: number;
// //     readonly instagram: string | null;
// //     readonly twitter: string | null;
// //     readonly link: string;
// //     readonly name: string;
// //     readonly numberOfSubscribers: string | null;
// //     readonly tags: string[];
// // }

// // interface IIcons {
// //     readonly label: string;
// //     readonly icon: string;
// //     readonly link: string;
// // }

// export const getIcons = ({ data }: any) => {
//     const { instagram, facebook, twitter, github } = data;
//     const icons = [];

//     if (instagram) {
//         icons.push({
//             icon: FiInstagram,
//             link: instagram,
//             label: "Instagram",
//         });
//     }

//     if (twitter) {
//         icons.push({
//             icon: BsTwitter,
//             link: twitter,
//             label: "Twitter",
//         });
//     }
//     if (facebook) {
//         icons.push({
//             icon: MdFacebook,
//             link: facebook,
//             label: "Facebook",
//         });
//     }
//     if (github) {
//         icons.push({
//             icon: AiFillGithub,
//             link: github,
//             label: "Github",
//         });
//     }

//     return icons;
// };

// export const getButtons = ({ data }: any) => {
//     const { blog, courses } = data;
//     const buttons = [];

//     if (blog) {
//         buttons.push({
//             link: blog,
//             label: "Blog",
//         });
//     }

//     if (courses) {
//         buttons.push({
//             link: courses,
//             label: "Courses",
//         });
//     }

//     return buttons;
// };
