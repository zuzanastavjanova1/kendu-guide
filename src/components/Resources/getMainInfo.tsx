import React from "react";
import CssMainInfo from "./CssMainInfo";
import GitMainInfo from "./GitMainInfo";
import HtmlMainInfo from "./HtmlMainInfo";
import JavascriptMainInfo from "./JavascriptMainInfo";
import ReactMainInfo from "./ReactMainInfo";

export const getMainInfo = (slug: string): React.ReactNode | null => {
    if (!slug) {
        return null;
    }
    return {
        html: <HtmlMainInfo />,
        css: <CssMainInfo />,
        javascript: <JavascriptMainInfo />,
        react: <ReactMainInfo />,
        git: <GitMainInfo />,
    }[slug];
};
