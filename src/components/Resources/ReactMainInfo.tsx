import React from "react";
import { Text, Heading } from "@chakra-ui/react";

const ReactMainInfo = () => {
    return (
        <>
            <Heading as="h3" size="md" mb="8">
                Welcome in React page
            </Heading>
            <Text fontSize="sm" pb="4">
                Here you will find a lot of articles, links and information
                created for you by developers from all over the world. Look for
                inspiration in others and explore the possibilities that this
                library can do.
            </Text>
            <Text fontSize="sm" pb="4" fontWeight="medium">
                This page also includes assignments that you can create and
                share with us. Feel free to contact us and get feedback that
                will move you forward.
            </Text>
        </>
    );
};

export default ReactMainInfo;
