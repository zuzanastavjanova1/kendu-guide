export enum ResourcesLevel {
    BASIC = "basic",
    ADVANCE = "advance",
    PRO = "pro",
}

export enum ResourcesFeatures {
    KEYWORDS = "keywords",
    QUESTIONS = "questions",
    ARTICLES = "articles",
}

export enum Sections {
    HTML = "html",
    CSS = "css",
    JAVASCRIPT = "javascript",
    GIT = "git",
    REACT = "react",
}

export enum ProgressStatus {
    IN_PROGRESS = "inProgress",
    DONE = "DONE",
}
