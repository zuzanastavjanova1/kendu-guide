import React, { FC } from "react";
import { Text, Heading, Center } from "@chakra-ui/react";
import { Dots } from "../Navigation/Dots";

const CssMainInfo: FC = () => {
    return (
        <>
            <Heading as="h3" size="md" mb="8">
                CSS part
            </Heading>
            <Text fontSize="sm" pb="4">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                Praesent vitae arcu tempor neque lacinia pretium. Ut tempus
                purus at lorem. Quisque tincidunt scelerisque libero. Integer in
                sapien. Phasellus enim erat, vestibulum vel, aliquam a, posuere
                eu, velit. Pellentesque arcu. In sem justo, commodo ut, suscipit
                at, pharetra vitae, orci. In convallis. Curabitur bibendum justo
                non orci. In sem justo, commodo ut, suscipit at, pharetra vitae,
                orci.
            </Text>
            <Text fontSize="sm" pb="4" fontWeight="medium">
                It recommend 10 out of 10 developers.
            </Text>
            <Text fontSize="sm" pb="2">
                Etiam egestas wisi a erat. Etiam commodo dui eget wisi.
                Temporibus autem quibusdam et aut officiis debitis aut rerum
                necessitatibus saepe eveniet ut et voluptates repudiandae sint
                et molestiae non recusandae. Maecenas sollicitudin. Etiam
                posuere lacus quis dolor. Sed elit dui, pellentesque a, faucibus
                vel, interdum nec, diam.
            </Text>
            <Center>
                <Dots />
            </Center>

            <Center>
                <Text fontSize="sm" py="4" w="80%" align="center">
                    Aliquam ante. Donec vitae arcu. Temporibus autem quibusdam
                    et aut officiis debitis aut rerum necessitatibus saepe
                    eveniet ut et voluptates repudiandae sint et molestiae non
                    recusandae.
                </Text>
            </Center>
        </>
    );
};

export default CssMainInfo;
