import React from "react";
import { Text, Heading, Center } from "@chakra-ui/react";
import { Dots } from "../Navigation/Dots";

const JavascriptMainInfo = () => {
    return (
        <>
            <Heading as="h3" size="md" mb="8">
                Javascript part
            </Heading>
            <Text fontSize="sm" pb="4">
                Git is an everyday part of programming and a very powerful tool
                that can make life easier for both you and your colleagues. If
                one understands the principles of git, the work of him and his
                team will always be safe and more effective.
            </Text>
            <Text fontSize="sm" pb="4" fontWeight="medium">
                It recommend 10 out of 10 developers.
            </Text>
            <Text fontSize="sm" pb="2">
                We recommend using one of the tools you can see below, where you
                can work easier and your work will be clear.
            </Text>
            <Text fontSize="sm" pb="2">
                With a simple project that is programmed by only one person,
                there is no need to use various principles of git, but we
                recommend it for your own clarity. Basic commands via the
                terminal are enough and it is good to try them all. However, if
                you already work in a team, it is necessary to use one of the
                tools to maintain transparency and efficiency.
            </Text>
            <Center>
                <Dots />
            </Center>

            <Center>
                <Text fontSize="sm" py="4" w="80%" align="center">
                    The use of git will be on the first two projects (HTML / CSS
                    and Javascript) at the basic level. Just display the code on
                    github / gitlab. For the React project, it will be necessary
                    to follow the principles of git and rules which will be
                    described for the project. Only in this way do we mock
                    reality and this skills will come in handy for you in the
                    future and you will appreciate it.
                </Text>
            </Center>
        </>
    );
};

export default JavascriptMainInfo;
