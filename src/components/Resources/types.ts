import { Sections } from "./constants";

export interface IResources {
    readonly userId: string;
    readonly sectionLevel: string;
    readonly filteredSection: Sections;
    readonly section: string;
}

export interface IResource {
    readonly isItemSelected: boolean;
    readonly userId: string;
}
