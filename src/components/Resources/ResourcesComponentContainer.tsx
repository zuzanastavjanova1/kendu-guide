import React, { FC } from "react";
import { Text, Heading, Flex, Spacer } from "@chakra-ui/react";

interface IResourcesComponentContainer {
    readonly children: React.ReactNode;
    readonly section: string;
    readonly title: string;
    readonly userSavedIds: string[];
    readonly totalNumberOfItems: number;
}

// TODO need to be fix - changed database structure broke it

const ResourcesComponentContainer: FC<IResourcesComponentContainer> = ({
    children,
    section,
    title,
    userSavedIds,
    totalNumberOfItems,
}) => {
    return (
        <>
            <Flex mb="6" mt="10">
                <Flex>
                    <Heading as="h3" size="sm" pr="2" textTransform="uppercase">
                        {section}
                    </Heading>
                    <Heading as="h3" size="sm">
                        {title}
                    </Heading>
                </Flex>

                <Spacer />
                <Text fontSize="14" fontWeight="medium" color="brand.lila">
                    {/* need to be fixed after change data structure */}
                    saved {userSavedIds ?? 0} from {totalNumberOfItems}
                </Text>
            </Flex>
            {children}
        </>
    );
};

export default ResourcesComponentContainer;
