import React from "react";
import { Text, Heading, Center } from "@chakra-ui/react";
import { Dots } from "../Navigation/Dots";

const HtmlMainInfo = () => {
    return (
        <>
            <Heading as="h3" size="md" mb="8">
                Lets get touch with HTML
            </Heading>
            <Text fontSize="sm" pb="4">
                HTML (Hypertext Markup Language) is a markup language that
                defines the structure of your content. We can imagine it as a
                human skeleton, without which we cannot do without it, and even
                though we dont talk about it much among the developers, because
                we all expect a little bit that we know it and we know about its
                important role.
            </Text>
            <Text fontSize="sm" pb="2">
                You can find out lot of resources and articles about html a its
                not important know definitions, but important is can use it,
                know differences of tags and be familier with html.
            </Text>

            <Center>
                <Dots />
            </Center>

            <Center>
                <Text fontSize="sm" py="4" w="80%" align="center">
                    You will use html daily as part of the frontend developmen
                    and after a while you do not have to mark it as a friend for
                    one night, but your best friend.
                </Text>
            </Center>
        </>
    );
};

export default HtmlMainInfo;
