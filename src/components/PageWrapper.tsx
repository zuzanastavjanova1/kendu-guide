import { Box } from "@chakra-ui/react";
import React, { FC } from "react";

export const PageWrapper: FC<{ children: React.ReactNode }> = ({
    children,
}) => (
    <Box mx="5.6rem" pt="6rem" minH="100vh">
        {children}
    </Box>
);
