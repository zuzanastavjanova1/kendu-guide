import { IconType } from "react-icons/lib";
import { MdRadioButtonUnchecked, MdRadioButtonChecked } from "react-icons/md";
import {
    ProgressStatus,
    ResourcesLevel,
    Sections,
} from "../Resources/constants";

export const getTopicColor = (title: Sections): string => {
    return {
        [Sections.HTML]: "brand.green",
        [Sections.CSS]: "brand.lila",
        [Sections.JAVASCRIPT]: "brand.yellow",
        [Sections.GIT]: "brand.green",
        [Sections.REACT]: "brand.yellow",
    }[title];
};

export const getProgressIcon = (step: ProgressStatus): IconType => {
    return {
        [ProgressStatus.IN_PROGRESS]: MdRadioButtonUnchecked,
        [ProgressStatus.DONE]: MdRadioButtonChecked,
    }[step];
};

export const getProgressIconSize = (level: ResourcesLevel): number => {
    return {
        [ResourcesLevel.BASIC]: 4,
        [ResourcesLevel.ADVANCE]: 5,
        [ResourcesLevel.PRO]: 6,
    }[level];
};
