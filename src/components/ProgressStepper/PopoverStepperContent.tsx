import {
    PopoverArrow,
    PopoverBody,
    PopoverCloseButton,
    PopoverContent,
    PopoverHeader,
    Text,
    Box,
} from "@chakra-ui/react";
import Link from "next/link";
import React, { FC } from "react";
import Topics from "./Topics";
import { ITopics } from "./types";

interface IPopoverStepperContent {
    readonly topics: ITopics[];
    readonly hashLink: string;
    readonly level: string;
    readonly title: string;
}

const PopoverStepperContent: FC<IPopoverStepperContent> = ({
    topics,
    hashLink,
    level,
    title,
}) => {
    const linkTitle = (
        <Text fontWeight="bold" ml="2">
            {level.toUpperCase()}
        </Text>
    );

    return (
        <>
            <PopoverContent
                width="25rem"
                _focus={{
                    outline: 0,
                }}
            >
                <PopoverArrow />
                <Link href={`resources/${hashLink}`}>
                    <PopoverHeader
                        _hover={{
                            cursor: "pointer",
                            color: "brand.lila",
                            transition: "all 0.5s ease",
                        }}
                    >
                        <Box display="flex">
                            Go to {title} {linkTitle}
                        </Box>
                    </PopoverHeader>
                </Link>
                <PopoverCloseButton />
                <PopoverBody>
                    <Topics topics={topics} />
                </PopoverBody>
            </PopoverContent>
        </>
    );
};

export default PopoverStepperContent;
