import { Box, Button, Text, Tooltip } from "@chakra-ui/react";
import React, { useState, FC } from "react";
import { useSelectUniqueIds } from "../../hooks/useSelectUniqueIds";
import { updateUserSavedTopics } from "../../services/useUser";
import { ITopic } from "./types";

const Topic: FC<ITopic> = ({
    topic,
    userId,
    userSavedTopicsIds,
    selectedTopic,
}) => {
    const [checked, setChecked] = useState(topic.checkedStatus);

    const handleChangeCheckedStatus = async (topic: any) => {
        const newUsersSavedIds = [...userSavedTopicsIds, topic.id];
        const cleanIdsList = useSelectUniqueIds(newUsersSavedIds);

        if (cleanIdsList) {
            await updateUserSavedTopics({ userId, ids: cleanIdsList });
        }
    };

    const handleKeywordClick = () => {
        setChecked(!checked);
        handleChangeCheckedStatus(topic);
    };
    return (
        <Box
            key={topic.id}
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            mb="2"
        >
            <Text fontSize="sm">{topic.title}</Text>
            <Tooltip
                hasArrow
                label={
                    selectedTopic
                        ? "Click if you want still study this topic"
                        : "Click if your are ready to move to another topic"
                }
                bg="brand.green"
                color="white"
                placement="right"
            >
                <Button onClick={handleKeywordClick} size="sm">
                    {selectedTopic ? "Done" : "Still studying"}
                </Button>
            </Tooltip>
        </Box>
    );
};

export default Topic;
