import {
    Box,
    Popover,
    PopoverTrigger,
    Tooltip,
    Divider,
    Center,
    Text,
    keyframes,
} from "@chakra-ui/react";
import React, { FC } from "react";
import { useSections } from "../../services/useSections";
import PopoverStepperContent from "./PopoverStepperContent";
import ProgressIcon from "./ProgressIcon";
import { getTopicColor } from "./helpers";
import { ILevels } from "./types";
import ProgressBarLabel from "./ProgressBarLabel";
import { ResourcesLevel } from "../Resources/constants";

interface IProgressStepper {
    readonly isOpenNav: boolean;
}

const visibility = keyframes`
  from {opacity: 0}
  to {opacity: 1}
`;

const ProgressStepper: FC<IProgressStepper> = ({ isOpenNav }) => {
    const sectionsData = useSections();
    const visibilityAnimation = `${visibility} 1s linear`;

    return (
        <>
            {sectionsData?.map((section) => (
                <Tooltip
                    key={section.name}
                    hasArrow
                    label={section.name}
                    bg={getTopicColor(section.name)}
                    color="white"
                    placement="right"
                >
                    <Box key={section.name} mb="2rem">
                        {section.levels.map((levelItem: ILevels) => {
                            const hashLink = `${section.name}#${levelItem.level}`;
                            return (
                                <Box display="flex" key={levelItem.level}>
                                    <Box width={isOpenNav ? "100%" : "100%"}>
                                        <Popover>
                                            <PopoverTrigger>
                                                <Box
                                                    display="flex"
                                                    px={
                                                        isOpenNav ? "1rem" : "0"
                                                    }
                                                    alignItems="start"
                                                    justifyContent={
                                                        isOpenNav
                                                            ? "space-between"
                                                            : "center"
                                                    }
                                                >
                                                    {isOpenNav && (
                                                        <ProgressBarLabel
                                                            isOpenNav={
                                                                isOpenNav
                                                            }
                                                            visibilityAnimation={
                                                                visibilityAnimation
                                                            }
                                                            section={section}
                                                            levelItem={
                                                                levelItem
                                                            }
                                                        />
                                                    )}
                                                    <Box
                                                        mt="2"
                                                        display="grid"
                                                        justifyContent="center"
                                                        width={
                                                            isOpenNav
                                                                ? "4rem"
                                                                : "auto"
                                                        }
                                                        _hover={{
                                                            cursor: "pointer",
                                                        }}
                                                    >
                                                        <ProgressIcon
                                                            step={
                                                                levelItem.step
                                                            }
                                                            level={
                                                                levelItem.level
                                                            }
                                                            color={getTopicColor(
                                                                section.name
                                                            )}
                                                        />
                                                        {levelItem.level !==
                                                            ResourcesLevel.PRO && (
                                                            <Center
                                                                height="1.5rem"
                                                                mt="2"
                                                            >
                                                                <Divider
                                                                    orientation="vertical"
                                                                    borderColor={getTopicColor(
                                                                        section.name
                                                                    )}
                                                                />
                                                            </Center>
                                                        )}
                                                    </Box>
                                                </Box>
                                            </PopoverTrigger>
                                            <PopoverStepperContent
                                                hashLink={hashLink}
                                                topics={
                                                    levelItem?.topics as any
                                                }
                                                level={levelItem.level}
                                                title={section.name}
                                            />
                                        </Popover>
                                    </Box>
                                </Box>
                            );
                        })}
                    </Box>
                </Tooltip>
            ))}
        </>
    );
};

export default ProgressStepper;
