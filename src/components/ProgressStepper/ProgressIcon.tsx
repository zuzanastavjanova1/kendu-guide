import React, { FC } from "react";
import { Icon } from "@chakra-ui/react";
import { getProgressIconSize, getProgressIcon } from "./helpers";
import { ProgressStatus, ResourcesLevel } from "../Resources/constants";

interface IProgressIcon {
    readonly step: ProgressStatus;
    readonly level: ResourcesLevel;
    readonly color: string;
}

const ProgressIcon: FC<IProgressIcon> = ({ step, level, color }) => (
    <Icon
        w={getProgressIconSize(level)}
        h={getProgressIconSize(level)}
        display="flex"
        justifyContent="center"
        as={getProgressIcon(step)}
        aria-label={step}
        color={color}
    />
);

export default ProgressIcon;
