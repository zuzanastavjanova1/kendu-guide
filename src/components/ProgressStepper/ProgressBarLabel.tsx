import { Box, Text } from "@chakra-ui/react";
import React, { FC } from "react";
import { Sections } from "../Resources/constants";
import { ILevels } from "./types";

interface IProgressBarLabel {
    readonly isOpenNav: boolean;
    readonly visibilityAnimation: string;
    readonly section: any;
    readonly levelItem: ILevels;
}

const ProgressBarLabel: FC<IProgressBarLabel> = ({
    isOpenNav,
    visibilityAnimation,
    section,
    levelItem,
}) => {
    console.log(section, "se");
    return (
        <Box
            display="flex"
            width="100%"
            pl="1rem"
            pt="0.4rem"
            alignItems="center"
            animation={isOpenNav ? visibilityAnimation : undefined}
            transition={isOpenNav ? "all 0.5s ease" : "all 0.1s ease"}
            opacity={isOpenNav ? "1" : "0"}
            _hover={{
                cursor: "pointer",
            }}
        >
            <Text fontSize="sm" pr="1" color="silver">
                {section.shortName}
            </Text>
            <Text color="silver">{levelItem.level}</Text>
        </Box>
    );
};

export default ProgressBarLabel;
