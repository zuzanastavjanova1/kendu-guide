import { ResourcesLevel, ProgressStatus } from "./../Resources/constants";

export interface ITopics {
    readonly checkedStatus: boolean;
    readonly description: string;
    readonly id: number;
    readonly title: string;
}

export interface ILevels {
    readonly id: number;
    readonly level: ResourcesLevel;
    readonly step: ProgressStatus;
    readonly topics: ITopics[];
}

export interface ITopic {
    readonly topic: ITopics;
    readonly userId: string;
    readonly userSavedTopicsIds: string[];
    readonly selectedTopic: ITopics;
}
