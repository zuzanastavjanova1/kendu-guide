import {
    ButtonGroup,
    useEditableControls,
    Flex,
    EditableInput,
    Editable,
} from "@chakra-ui/react";
import React, { useState, FC } from "react";
import { CheckIcon, CloseIcon, EditIcon } from "@chakra-ui/icons";
import { IconButton } from "@chakra-ui/react";
import { updateName } from "../services/useUser";

interface IName {
    readonly userName?: string;
    readonly userId: string;
}

export const EditableName: FC<IName> = ({ userName, userId }) => {
    const [newName, setNewName] = useState<string | null>(userName || null);

    const EditableControls = () => {
        const {
            isEditing,
            getSubmitButtonProps,
            getCancelButtonProps,
            getEditButtonProps,
        } = useEditableControls();

        return isEditing ? (
            <ButtonGroup justifyContent="center" size="sm">
                <IconButton
                    aria-label="Check"
                    onSubmit={() => updateName({ userId, userName: newName })}
                    icon={<CheckIcon />}
                    {...getSubmitButtonProps()}
                />
                <IconButton
                    aria-label="Close"
                    icon={<CloseIcon />}
                    {...getCancelButtonProps()}
                />
            </ButtonGroup>
        ) : (
            <Flex justifyContent="center" ml="4">
                <IconButton
                    aria-label="Edit"
                    size="sm"
                    icon={<EditIcon />}
                    {...getEditButtonProps()}
                />
            </Flex>
        );
    };

    return (
        <Editable
            textAlign="center"
            display="flex"
            placeholder={userName ?? "Your name"}
            isPreviewFocusable={false}
            onChange={(e) => setNewName(e)}
            onSubmit={() => updateName({ userId, userName: newName })}
        >
            <EditableInput mr="4" />
            <EditableControls />
        </Editable>
    );
};
