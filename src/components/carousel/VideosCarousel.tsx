export {};
// import React, { useState } from "react";
// import {
//     Box,
//     Heading,
//     Text,
//     Tag,
//     HStack,
//     SimpleGrid,
//     Button,
//     IconButton,
//     Link,
//     Tooltip,
//     Center,
//     Wrap,
// } from "@chakra-ui/react";
// import { AiOutlineStar, AiFillStar } from "react-icons/ai";
// import { MdKeyboardArrowRight, MdKeyboardArrowLeft } from "react-icons/md";
// import { getIcons } from "../Videos/getIcons";

// import { updateUserSavedVideos } from "../../services/useVideos";
// import { useSelectUniqueIds } from "../../hooks/useSelectUniqueIds";
// import { useAuth } from "../../../context/AuthUserContext";

// const VideosCarousel = ({ images }: any) => {
//     const { userVideos, userId } = useAuth();
//     const numberOfViewCard = 4;
//     const useSavedVideosQuestions = userVideos?.map((i: string) => i) ?? [];
//     const [currentImageIdx, setCurrentImagIdx] = useState(0);

//     const prevSlide = () => {
//         // find out whether currentImageIdx eqals 0 and thus user reached beginning of carousel
//         const resetToVeryBack = currentImageIdx === 0;

//         const index = resetToVeryBack ? images.length - 1 : currentImageIdx - 1;

//         // assign the logical index to current image index that will be used in render method
//         setCurrentImagIdx(index);
//     };

//     const nextSlide = () => {
//         // check if we need to start over from the first index
//         const resetIndex = currentImageIdx === images.length - 1;

//         const index = resetIndex ? 0 : currentImageIdx + 1;

//         // assign the logical index to current image index that will be used in render method
//         setCurrentImagIdx(index);
//     };

//     // create a new array with 5 elements from the source images
//     const activeImageSourcesFromState = images.slice(
//         currentImageIdx,
//         currentImageIdx + numberOfViewCard
//     );

//     // check the length of the new array (it’s less than 5 when index is at the end of the imagge sources array)
//     const imageSourcesToDisplay =
//         activeImageSourcesFromState.length < numberOfViewCard
//             ? // if the imageSourcesToDisplay's length is lower than 5 images than append missing images from the beginning of the original array
//               [
//                   ...activeImageSourcesFromState,
//                   ...images.slice(
//                       0,
//                       numberOfViewCard - activeImageSourcesFromState.length
//                   ),
//               ]
//             : activeImageSourcesFromState;

//     const handleSaveQuestion = async (item: any) => {
//         const newUsersSavedIds = [...useSavedVideosQuestions, item.id];
//         const cleanIdsList = useSelectUniqueIds(newUsersSavedIds);

//         if (cleanIdsList) {
//             await updateUserSavedVideos({
//                 userId: userId as string,
//                 ids: cleanIdsList,
//             });
//         }
//     };

//     return (
//         <Center>
//             <IconButton
//                 as={MdKeyboardArrowLeft}
//                 aria-label="next"
//                 mr="4"
//                 onClick={prevSlide}
//                 size="sm"
//                 bg="transparent"
//                 _hover={{
//                     cursor: "pointer",
//                 }}
//             />
//             <SimpleGrid columns={numberOfViewCard} spacingX="5" spacingY="5">
//                 {imageSourcesToDisplay.map((data: any) => {
//                     const selectedItem = useSavedVideosQuestions.includes(
//                         data.id
//                     );
//                     const tooltipLabel = (
//                         <Box p="4">
//                             <Text fontWeight="semibold" fontSize="md">
//                                 {data.name}
//                             </Text>
//                             <Text>{data.author}</Text>
//                             <Text mt="4">{data.description}</Text>
//                         </Box>
//                     );
//                     return (
//                         <Tooltip
//                             hasArrow
//                             label={tooltipLabel}
//                             bg="brand.yellow"
//                             color="black"
//                             placement="right"
//                             key={data.id}
//                         >
//                             <Box
//                                 key={data.id}
//                                 bg="white"
//                                 p="4"
//                                 borderRadius={6}
//                                 boxShadow="0 3px 10px rgb(0 0 0 / 0.1)"
//                             >
//                                 <Box display="flex" justifyContent="end">
//                                     <IconButton
//                                         as={
//                                             selectedItem
//                                                 ? AiFillStar
//                                                 : AiOutlineStar
//                                         }
//                                         aria-label="start"
//                                         size="xs"
//                                         bg="white"
//                                         onClick={() => handleSaveQuestion(data)}
//                                         _hover={{
//                                             cursor: "pointer",
//                                         }}
//                                     />
//                                 </Box>
//                                 <Link href={data.link} isExternal>
//                                     <Heading
//                                         as="h6"
//                                         fontSize="md"
//                                         mb="2"
//                                         noOfLines={1}
//                                     >
//                                         {data.name}
//                                     </Heading>
//                                 </Link>
//                                 <Text color="grey">{data.author ?? ""}</Text>

//                                 <Text mt="2" noOfLines={2} fontSize="sm">
//                                     {data.description}
//                                 </Text>
//                                 <Wrap spacing="2" mt="4">
//                                     {data.tags?.map((tag: string) => {
//                                         return (
//                                             <HStack spacing={2} key={tag}>
//                                                 <Tag size="sm">{tag}</Tag>
//                                             </HStack>
//                                         );
//                                     })}
//                                 </Wrap>
//                                 <Wrap spacing="2" mt="4">
//                                     <Button
//                                         variant="link"
//                                         size="xs"
//                                         color="brand.lila"
//                                     >
//                                         {data.courses && "Courses"}
//                                     </Button>
//                                     <Button
//                                         variant="link"
//                                         size="xs"
//                                         color="brand.lila"
//                                     >
//                                         {data.blog && "Blog"}
//                                     </Button>
//                                 </Wrap>
//                                 <Box display="flex" justifyContent="end">
//                                     <Wrap spacing="4" mt="2">
//                                         {getIcons({ data }).map((i) => {
//                                             return (
//                                                 <Link
//                                                     href={i.link}
//                                                     isExternal
//                                                     key={i.label}
//                                                 >
//                                                     <IconButton
//                                                         as={i.icon}
//                                                         size="xs"
//                                                         aria-label={i.label}
//                                                         bg="white"
//                                                         _hover={{
//                                                             cursor: "pointer",
//                                                         }}
//                                                     />
//                                                 </Link>
//                                             );
//                                         })}
//                                     </Wrap>
//                                 </Box>
//                             </Box>
//                         </Tooltip>
//                     );
//                 })}
//             </SimpleGrid>

//             <IconButton
//                 as={MdKeyboardArrowRight}
//                 aria-label="next"
//                 ml="4"
//                 onClick={nextSlide}
//                 size="sm"
//                 bg="transparent"
//                 _hover={{
//                     cursor: "pointer",
//                 }}
//             />
//         </Center>
//     );
// };

// export default VideosCarousel;
