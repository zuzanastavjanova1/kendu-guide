import React, { useState, FC, ContextType } from "react";
import { Box } from "@chakra-ui/react";

import { Header } from "./Navigation/Header";
import { Sidebar } from "./Navigation/Sidebar";
import { ArrowIconOpenNav } from "./Navigation/ArrowIconOpenNav";
import SidebarMainTopics from "./Navigation/SidebarMainTopics";
import SidebarPersonalTopics from "./Navigation/SidebarPersonalTopics";
import ProgressStepper from "./ProgressStepper";
import { getSession, GetSessionParams } from "next-auth/react";

const Container: FC<{ children: React.ReactNode }> = ({ children }) => {
    const [isLeftNavOpen, setLeftIsNavOpen] = useState(false);
    const [isRightNavOpen, setRightIsNavOpen] = useState(false);

    const [openedItem, setOpenedItem] = React.useState<number | null>(null);

    const handleQuestionClick = (id: number) => {
        if (openedItem === id) {
            setOpenedItem(null);
        } else {
            setOpenedItem(id);
        }
    };

    return (
        <Box bg="#F5F5F5" h="100%">
            <Header isNavOpen={isRightNavOpen} />
            <ArrowIconOpenNav
                isNavOpen={isLeftNavOpen}
                setIsNavOpen={setLeftIsNavOpen}
            />
            <Sidebar
                topChildren={
                    <SidebarMainTopics
                        isNavOpen={isLeftNavOpen}
                        handleQuestionClick={handleQuestionClick}
                        openedItem={openedItem}
                    />
                }
                bottomChildren={
                    <SidebarPersonalTopics
                        isNavOpen={isLeftNavOpen}
                        handleQuestionClick={handleQuestionClick}
                        openedItem={openedItem}
                    />
                }
                isNavOpen={isLeftNavOpen}
                leftSidebar
            />
            <Box
                height="100wh"
                marginLeft={isLeftNavOpen ? "14rem" : undefined}
                marginRight={isRightNavOpen ? "14rem" : undefined}
                transition="all 0.5s ease"
            >
                {children}
            </Box>
            <ArrowIconOpenNav
                isNavOpen={isRightNavOpen}
                setIsNavOpen={setRightIsNavOpen}
                rightSidebar
            />
            <Sidebar
                topChildren={<ProgressStepper isOpenNav={isRightNavOpen} />}
                isNavOpen={isRightNavOpen}
            />
        </Box>
    );
};

export default Container;

export async function getInitialProps(context: GetSessionParams | undefined) {
    const session = await getSession(context);

    return {
        props: {
            session,
        },
    };
}
