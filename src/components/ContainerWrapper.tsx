import { Box } from "@chakra-ui/react";
import React, { FC } from "react";

export const ContainerWrapper: FC<{ children: React.ReactNode }> = ({
    children,
}) => <Box p="2rem">{children}</Box>;
