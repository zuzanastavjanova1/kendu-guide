import React, { FC } from "react";

import {
    SandpackPreview,
    SandpackCodeEditor,
    SandpackLayout,
    SandpackReactDevTools,
    UnstyledOpenInCodeSandboxButton,
} from "@codesandbox/sandpack-react";

export const CustomSandpack: FC = () => (
    <SandpackLayout>
        <SandpackCodeEditor
            showTabs
            showLineNumbers={false}
            showInlineErrors
            wrapContent
            closableTabs
        />
        <UnstyledOpenInCodeSandboxButton>
            Open in CodeSandbox
        </UnstyledOpenInCodeSandboxButton>
        <SandpackPreview
            actionsChildren={
                <button
                    className="sp-button"
                    style={{
                        padding: "1rem 2rem",
                    }}
                    onClick={() => window.alert("Bug reported!")}
                >
                    Report bug
                </button>
            }
        />
        <div style={{ display: "flex", width: "100%", height: "20rem" }}>
            <SandpackReactDevTools />
        </div>

        {/* <SandpackCodeViewer /> */}
    </SandpackLayout>
);
