import React, { FC } from "react";
import { useSandpack } from "@codesandbox/sandpack-react";

export const SimpleCodeViewer: FC = () => {
    const { sandpack } = useSandpack();
    const { files, activeFile } = sandpack;

    const code = files[activeFile].code;
    return <pre>{code}</pre>;
};
