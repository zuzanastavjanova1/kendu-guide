import { Grid } from "@chakra-ui/react";
import React, { FC } from "react";
import { useFilteredResultsData } from "../../hooks/useFilteredResultsData";
import { useIsIdAmongUsersIds } from "../../hooks/userIsIdAmongUserIds";
import { useResourcesData } from "../../services/data/useResourcesData";
import useUsersSavedIds from "../../services/useUsersSavedIds";
import { ResourcesFeatures } from "../Resources/constants";
import ResourcesComponentContainer from "../Resources/ResourcesComponentContainer";
import { IResources } from "../Resources/types";
import { Article } from "./Article";

export const Articles: FC<IResources> = ({
    filteredSection,
    userId,
    section,
    sectionLevel,
}) => {
    const userSavedIds = useUsersSavedIds(userId);

    const { data, isLoading } = useResourcesData({
        filteredSection,
    });

    // TODO dont like this handle data - the database structure should be different
    // change the logic for handle user data
    const allArticles = data?.articles;

    if (isLoading) {
        // TODO create a loader
        return <div>...loading</div>;
    }

    if (!allArticles || userSavedIds?.articles == null) {
        return null;
    }

    const resultsData = useFilteredResultsData({
        data: allArticles,
        sectionLevel,
    });

    return (
        <ResourcesComponentContainer
            section={section}
            title={ResourcesFeatures.ARTICLES}
            userSavedIds={userSavedIds?.keywords?.length}
            totalNumberOfItems={Object.entries(allArticles).length}
        >
            <Grid templateColumns="repeat(5, 1fr)">
                {resultsData?.map((article) => {
                    const isItemSelected = useIsIdAmongUsersIds({
                        data: userSavedIds?.articles,
                        id: article.id,
                    });

                    return (
                        <Article
                            key={article.id}
                            userId={userId}
                            article={article}
                            isItemSelected={isItemSelected}
                        />
                    );
                })}
            </Grid>
        </ResourcesComponentContainer>
    );
};
