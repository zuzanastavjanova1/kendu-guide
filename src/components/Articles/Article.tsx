import React, { useState, FC } from "react";
import { Box, Text, Link, Tooltip, Button, Icon } from "@chakra-ui/react";
import { Card } from "../Card";
import { BsCheckLg, BsCheckCircleFill } from "react-icons/bs";
import { useUpdateUsersResourcesItem } from "../../services/data/useUpdateUsersResourcesItem";
import { IResource } from "../Resources/types";
import { IArticle } from "./types";
import { ResourcesFeatures } from "../Resources/constants";

interface IArticlesProps extends IResource {
    readonly article: IArticle;
}

export const Article: FC<IArticlesProps> = ({
    article,
    isItemSelected,
    userId,
}) => {
    const [checked, setChecked] = useState(isItemSelected);

    const { mutation } = useUpdateUsersResourcesItem({
        userId,
        itemId: article.id,
        resources: ResourcesFeatures.ARTICLES,
    });

    return (
        <Card key={article.id} small width="100%">
            <Box display="grid">
                <Box display="flex" justifyContent="end">
                    <Tooltip
                        hasArrow
                        label="Click if you already read this article"
                        width="15rem"
                        textAlign="center"
                        bg="brand.yellow"
                        color="white"
                        placement="top-end"
                    >
                        <Button
                            width="fit-content"
                            aria-label="check"
                            size="xs"
                            position="relative"
                            bottom="0.8rem"
                            left="0.8rem"
                            bg="white"
                            onClick={async () => {
                                await mutation.mutate();
                                await setChecked(!checked);
                            }}
                        >
                            <Icon
                                as={checked ? BsCheckLg : BsCheckCircleFill}
                            />
                        </Button>
                    </Tooltip>
                </Box>
                <Tooltip
                    hasArrow
                    label={article.name}
                    bg="brand.green"
                    color="white"
                    placement="right"
                >
                    <Link
                        href={article.link}
                        isExternal
                        _hover={{
                            textDecoration: "none",
                        }}
                    >
                        <Text
                            noOfLines={2}
                            height="4.2rem"
                            mb="2"
                            _hover={{
                                color: "brand.lila",
                            }}
                        >
                            {article.name}
                        </Text>
                    </Link>
                </Tooltip>
            </Box>
        </Card>
    );
};
