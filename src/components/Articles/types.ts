export interface IArticle {
    readonly id: string;
    readonly level: string;
    readonly link: string;
    readonly name: string;
    readonly section: string;
}
