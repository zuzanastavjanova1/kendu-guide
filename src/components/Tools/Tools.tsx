export {};
// import React, { FC } from "react";
// import { Heading, Grid, Badge, Box } from "@chakra-ui/react";
// import { useCollection } from "../../services/data/useCollection";
// import { Tool } from "./Tool";

// interface ITools {
//     readonly title: string;
//     readonly nameCollection: string;
// }

// export const Tools: FC<ITools> = ({ title, nameCollection }) => {
//     const { data, isLoading } = useCollection(nameCollection);

//     if (isLoading) {
//         return <div>Loading...</div>;
//     }

//     return (
//         <>
//             <Box display="flex" my="6">
//                 <Heading as="h3" size="sm">
//                     {title}
//                 </Heading>
//                 <Badge colorScheme="purple" ml="6">
//                     Recommended
//                 </Badge>
//             </Box>
//             <Grid
//                 templateColumns={["1fr", "repeat(2, 1fr)", "repeat(4, 1fr)"]}
//                 gap={6}
//             >
//                 {data?.map((item) => (
//                     // eslint-disable-next-line @typescript-eslint/ban-ts-comment
//                     // @ts-ignore
//                     <Tool item={item} key={item.id} />
//                 ))}
//             </Grid>
//         </>
//     );
// };
