export {};
// import React, { FC } from "react";
// import { Card } from "../Card";
// import {
//     Wrap,
//     WrapItem,
//     Link,
//     Button,
//     Heading,
//     Tag,
//     GridItem,
//     Box,
//     IconButton,
// } from "@chakra-ui/react";
// import { StarIcon } from "@chakra-ui/icons";
// import { ITool } from "../../services/useTools";

// interface IToolProps {
//     readonly item: ITool;
// }

// export const Tool: FC<IToolProps> = ({ item }) => {
//     return (
//         <GridItem>
//             <Card>
//                 <Box>
//                     <Box display="flex" justifyContent="space-between">
//                         <Heading as="h5" fontSize="lg">
//                             {item.item}
//                         </Heading>
//                         <IconButton
//                             aria-label="Save tool"
//                             icon={<StarIcon />}
//                             ml="4"
//                             h="6"
//                             minW="6"
//                             variant="ghost"
//                         />
//                     </Box>

//                     <Link href={item.link} isExternal>
//                         <Button variant="link" color="brand.lila" fontSize="sm">
//                             Website
//                         </Button>
//                     </Link>
//                     <Wrap mt="4">
//                         {item.tags.map((tag) => (
//                             <WrapItem key={tag}>
//                                 <Tag
//                                     size="sm"
//                                     key={tag}
//                                     variant="outline"
//                                     color="brand"
//                                 >
//                                     {tag}
//                                 </Tag>
//                             </WrapItem>
//                         ))}
//                     </Wrap>
//                 </Box>
//             </Card>
//         </GridItem>
//     );
// };
