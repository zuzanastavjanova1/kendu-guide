import { Box, Heading, Flex } from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { FC } from "react";
import { useAuth } from "../../context/AuthUserContext";

interface IPageSection {
    readonly title: string;
    readonly section: string;
    readonly children: React.ReactNode;
    readonly scrollTo: string;
}

const PageSection: FC<IPageSection> = ({
    children,
    title,
    scrollTo,
    section,
}) => {
    return (
        <Box id={scrollTo} mt="16" p="2rem">
            <Flex>
                <Heading
                    as="h4"
                    size="md"
                    mb="8"
                    textTransform="uppercase"
                    color="brand.green"
                >
                    {section} -
                </Heading>
                <Heading as="h4" size="md" mb="8" color="brand.green">
                    {title}
                </Heading>
            </Flex>

            {children}
        </Box>
    );
};

export default PageSection;
