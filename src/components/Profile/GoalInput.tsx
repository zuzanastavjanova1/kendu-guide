import { CheckIcon, CloseIcon, EditIcon } from "@chakra-ui/icons";
import {
    ButtonGroup,
    useEditableControls,
    IconButton,
    Editable,
    Flex,
    EditablePreview,
    EditableInput,
} from "@chakra-ui/react";
import React, { useState, FC } from "react";
import { updateMyGoal } from "../../services/useUser";
import { IGoalInput } from "../Navigation/types";

export const GoalInput: FC<IGoalInput> = ({ userGoal, userId }) => {
    const [goal, setGoal] = useState<string | null>(userGoal || null);

    function EditableControls() {
        const {
            isEditing,
            getSubmitButtonProps,
            getCancelButtonProps,
            getEditButtonProps,
        } = useEditableControls();

        return isEditing ? (
            <ButtonGroup justifyContent="center" size="sm">
                <IconButton
                    aria-label="check"
                    icon={<CheckIcon />}
                    onSubmit={() => updateMyGoal({ userId, userGoal: goal })}
                    {...getSubmitButtonProps()}
                />
                <IconButton
                    aria-label="close"
                    icon={<CloseIcon />}
                    {...getCancelButtonProps()}
                />
            </ButtonGroup>
        ) : (
            <Flex justifyContent="center" ml="4">
                <IconButton
                    aria-label="edit"
                    size="sm"
                    icon={<EditIcon />}
                    {...getEditButtonProps()}
                />
            </Flex>
        );
    }

    return (
        <Editable
            display="flex"
            placeholder={userGoal ?? "I want became a ..."}
            isPreviewFocusable={false}
            onChange={(e: string | null) => setGoal(e)}
            onSubmit={() => updateMyGoal({ userId, userGoal: goal })}
        >
            <EditablePreview />
            <EditableInput />
            <EditableControls />
        </Editable>
    );
};
