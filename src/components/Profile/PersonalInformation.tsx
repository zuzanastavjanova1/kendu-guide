import React from "react";
import { Heading, Box, Text, Center } from "@chakra-ui/react";
import { EditableName } from "../EditableName";
import { GoalInput } from "./GoalInput";
import useUsersSavedIds from "../../services/useUsersSavedIds";

const UserName = ({ userId, data }: { userId: string; data: any }) => {
    return data?.name ? (
        <Box display="flex" alignItems="center" mb="1">
            <Text mr="4" color="grey">
                Name:
            </Text>
            <Text mr="2">{data.name}</Text>
            <EditableName userName={data.name} userId={userId} />
        </Box>
    ) : (
        <Box display="flex">
            <Text mr="2">Write your name please</Text>
            <EditableName userName={data?.name} userId={userId} />
        </Box>
    );
};

const UserEmail = ({ email }: { email: string }) => {
    return (
        <Box display="flex" mb="1">
            <Text mr="4" color="grey">
                Email:
            </Text>
            <Text>{email}</Text>
        </Box>
    );
};

const UserGoal = ({
    userGoal,
    userId,
}: {
    userGoal: string | null;
    userId: string;
}) => {
    return (
        <Box display="flex" alignItems="baseline">
            <Text mr="4" color="grey">
                My Goal:
            </Text>
            <GoalInput userGoal={userGoal} userId={userId} />
        </Box>
    );
};

const PersonalInformation = ({ session }: any) => {
    const userId = session?.user?.id;
    // const { data } = useUsersSavedIds(userId as string);

    return (
        <>
            <Center bg="brand.gradient" borderRadius="6px" mb="6">
                <Heading as="h3" size="md" py="4" color="white">
                    Personal information
                </Heading>
            </Center>
            {/* <UserName userId={userId as string} user={data} /> */}
            <UserEmail email={session?.user?.email as string} />
            {/* <UserGoal userGoal={userGoal} userId={userId as string} /> */}
        </>
    );
};

export default PersonalInformation;
