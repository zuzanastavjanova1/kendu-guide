import {
    Box,
    Button,
    Collapse,
    Heading,
    useDisclosure,
    List,
} from "@chakra-ui/react";
import React, { FC } from "react";
// import { ArrowForwardIcon } from "@chakra-ui/icons";
// import { useResourcesData } from "../../services/data/useResourcesData";

export const SavedQuestion: FC<{ userQuestionsIds?: string[] }> = ({
    userQuestionsIds,
}) => {
    const { isOpen, onToggle } = useDisclosure();

    //const { data, isLoading } = useResourcesData({ filteredSection: "html" });

    return (
        <>
            <Box display="flex" alignContent="center" mt="12">
                <Heading as="h3" size="md" mb="4">
                    Saved questions
                </Heading>
                <Button
                    onClick={onToggle}
                    size="xs"
                    ml="6"
                    bg="brand.green"
                    color="white"
                    border="none"
                >
                    {!isOpen ? "Hide questions" : "Show questions"}
                </Button>
            </Box>

            <List spacing={3}>
                <Collapse in={!isOpen} animateOpacity></Collapse>
            </List>
        </>
    );
};
