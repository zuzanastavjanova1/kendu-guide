export interface IQuestion {
    readonly id: string;
    readonly question: string;
    readonly section: string;
    readonly type: string;
    readonly visibility: boolean;
}
