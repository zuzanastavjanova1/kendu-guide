import { ListItem, IconButton, Tooltip } from "@chakra-ui/react";
import React, { FC } from "react";
import { AttachmentIcon, SmallAddIcon } from "@chakra-ui/icons";
import { useUpdateUsersResourcesItem } from "../../services/data/useUpdateUsersResourcesItem";
import { IResource } from "../Resources/types";
import { IQuestion } from "./types";
import { ResourcesFeatures } from "../Resources/constants";

interface IQuestionProps extends IResource {
    readonly question: IQuestion;
}

export const Question: FC<IQuestionProps> = ({
    isItemSelected,
    question,
    userId,
}) => {
    const [checked, setChecked] = React.useState(isItemSelected);
    const { mutation } = useUpdateUsersResourcesItem({
        userId,
        itemId: question.id,
        resources: ResourcesFeatures.QUESTIONS,
    });

    return (
        <ListItem mb="2" key={question.id} display="flex" alignItems="center">
            {question.question}

            <Tooltip
                hasArrow
                label={
                    checked
                        ? "You can find this question already in your profile. For unselect, click to icon."
                        : "Save to profile"
                }
                bg="brand.green"
                color="white"
                placement="top"
            >
                <IconButton
                    aria-label={checked ? "Item selected" : "Item unselected"}
                    icon={checked ? <AttachmentIcon /> : <SmallAddIcon />}
                    ml="4"
                    h={checked ? "5" : "6"}
                    minW={checked ? "5" : "6"}
                    variant="ghost"
                    onClick={async () => {
                        mutation.mutate();
                        await setChecked(!checked);
                    }}
                />
            </Tooltip>
        </ListItem>
    );
};
