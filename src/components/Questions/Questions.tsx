import { UnorderedList } from "@chakra-ui/react";
import React, { FC } from "react";

import { Card } from "../Card";
import { Question } from "./Question";
import { useResourcesData } from "../../services/data/useResourcesData";
import useUsersSavedIds from "../../services/useUsersSavedIds";
import { useFilteredResultsData } from "../../hooks/useFilteredResultsData";
import { IResources } from "../Resources/types";
import ResourcesComponentContainer from "../Resources/ResourcesComponentContainer";
import { ResourcesFeatures } from "../Resources/constants";

export const Questions: FC<IResources> = ({
    filteredSection,
    sectionLevel,
    userId,
    section,
}) => {
    const userSavedIds = useUsersSavedIds(userId);

    const { data, isLoading } = useResourcesData({
        filteredSection,
    });

    const allQuestions = data?.questions;

    if (isLoading) {
        return <div>...loading</div>;
    }

    if (!allQuestions || userSavedIds?.questions == null) {
        return null;
    }

    const resultsData = useFilteredResultsData({
        data: allQuestions,
        sectionLevel,
    });

    return (
        <ResourcesComponentContainer
            section={section}
            title={ResourcesFeatures.QUESTIONS}
            userSavedIds={userSavedIds?.questions?.length}
            totalNumberOfItems={Object.entries(allQuestions).length}
        >
            <Card width="100%">
                <UnorderedList>
                    {resultsData?.map((question) => {
                        const questionsData = userSavedIds?.questions;
                        const selectedItem = questionsData?.includes(
                            question.id
                        );

                        return (
                            <Question
                                key={question.id}
                                question={question}
                                userId={userId}
                                isItemSelected={selectedItem}
                            />
                        );
                    })}
                </UnorderedList>
            </Card>
        </ResourcesComponentContainer>
    );
};
