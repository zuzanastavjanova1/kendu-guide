export {};
// import { Heading, Text, Flex, Grid, Spacer } from "@chakra-ui/react";
// import React, { FC } from "react";
// import { useCollection } from "../../services/data/useCollection";
// import { Github } from "./Github";

// interface IGithub {
//     readonly title: string;
//     readonly nameCollection: string;
// }

// export const Githubs: FC<IGithub> = ({ title, nameCollection }) => {
//     const { data, isLoading } = useCollection(nameCollection);

//     if (isLoading) {
//         return <div>is loading ....</div>;
//     }

//     return (
//         <>
//             <Flex mb="6" mt="10">
//                 <Heading as="h3" size="sm">
//                     {title}
//                 </Heading>
//                 <Spacer />
//                 <Text fontSize="14" fontWeight="medium" color="brand.lila">
//                     {data?.length} github accounts
//                 </Text>
//             </Flex>
//             <Grid templateColumns={["1fr", null, "repeat(2, 1fr)"]}>
//                 {data?.map((item: any) => (
//                     <Github item={item} key={item.id} />
//                 ))}
//             </Grid>
//         </>
//     );
// };
