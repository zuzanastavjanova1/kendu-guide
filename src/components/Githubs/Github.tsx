export {};
// import React, { FC } from "react";
// import { Tooltip, Link, Box, Text, Icon } from "@chakra-ui/react";
// import { AiFillGithub } from "react-icons/ai";
// import { Card } from "../Card";
// import { IGithubs } from "../../services/useGithubs";

// interface IGithub {
//     readonly item: IGithubs;
// }

// export const Github: FC<IGithub> = ({ item }) => {
//     return (
//         <Link
//             href={item.link}
//             isExternal
//             key={item.id}
//             _hover={{
//                 textDecoration: "none",
//             }}
//         >
//             <Card width="100%">
//                 <Tooltip
//                     hasArrow
//                     label={item.description}
//                     bg="brand.green"
//                     color="white"
//                     placement="right"
//                     key={item.id}
//                 >
//                     <Box display="flex" alignItems="baseline">
//                         <Icon as={AiFillGithub} mr="2" />
//                         <Box>
//                             <Text
//                                 _hover={{
//                                     color: "brand.lila",
//                                 }}
//                             >
//                                 {item.name}
//                             </Text>
//                             <Text noOfLines={1} fontSize="sm">
//                                 {item.description}
//                             </Text>
//                         </Box>
//                     </Box>
//                 </Tooltip>
//             </Card>
//         </Link>
//     );
// };
