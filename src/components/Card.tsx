import React, { useState } from "react";
import { motion } from "framer-motion";

// TODO - test

export const Card = (props: any) => {
    const [active, setActive] = useState(false);

    return (
        <motion.div style={{ ...containerStyle }}>
            <motion.div
                onHoverStart={() => setActive(!active)}
                onHoverEnd={() => setActive(!active)}
                style={{
                    margin: props.small ? 8 : 10,
                    width: props.width,
                    padding: props.small ? 10 : 20,
                    borderRadius: 10,
                    backgroundColor: "#fff",
                    border: props.border,
                    boxShadow: active
                        ? "0 3px 10px rgb(0 0 0 / 0.1)"
                        : "0 3px 10px rgb(0 0 0 / 0.015)",
                }}
                animate={{ scale: active ? 1.005 : 1 }}
            >
                {props.children}
            </motion.div>
        </motion.div>
    );
};

const containerStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
};
