import React, { FC } from "react";
import { SimpleGrid, Box, Text } from "@chakra-ui/react";
import { PageWrapper } from "../PageWrapper";
import { InfoPanel } from "../InfoPanel";

interface IProjectContainer {
    readonly children: React.ReactNode;
    readonly motivation: string;
    readonly projectName: string;
}

export const ProjectContainer: FC<IProjectContainer> = ({
    children,
    motivation,
    projectName,
}) => {
    return (
        <PageWrapper>
            <Box p="3.2rem">
                <Text mb="2rem" fontSize="1.5rem" color="gray.600">
                    {projectName}
                </Text>
                <InfoPanel text="this is a text" />
                <Text my="1.6rem">{motivation}</Text>
                {children}
            </Box>
        </PageWrapper>
    );
};
