import React, { FC } from "react";
import { SimpleGrid, Box, Text } from "@chakra-ui/react";
import CollapsibleItem from "../CollapsibleItem";

export const ProjectTaskItem: FC<{ title: string; content: string }> = ({
    title,
    content,
}) => {
    return (
        <CollapsibleItem withoutMarginBottom title={title}>
            <SimpleGrid
                mt="1.6px"
                columns={[1, 2]}
                spacing={{ base: 2, lg: 4 }}
            >
                {content}
            </SimpleGrid>
        </CollapsibleItem>
    );
};
