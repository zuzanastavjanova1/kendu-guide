import React, { FC } from "react";
import { Box } from "@chakra-ui/react";
import { TitleWithBadge } from "./TitleWithBadge";
import { BasicText } from "./BasicText";

interface IHomeBlock {
    readonly title: string;
    readonly badgeText?: string;
    readonly description?: string;
    readonly children: React.ReactNode;
}

export const HomeBlock: FC<IHomeBlock> = ({
    title,
    badgeText,
    description,
    children,
}) => (
    <>
        <TitleWithBadge title={title} badgeText={badgeText} />
        <BasicText>{description}</BasicText>
        <Box pb="6">{children}</Box>
    </>
);
