import React, { FC } from "react";
import { Heading, Box, Badge } from "@chakra-ui/react";

export const TitleWithBadge: FC<{ title: string; badgeText?: string }> = ({
    title,
    badgeText,
}) => (
    <Box display="flex" alignItems="center">
        <Heading as="h3" size="sm">
            {title}
        </Heading>
        {badgeText && (
            <Badge colorScheme="purple" ml="6" py="1" px="2" borderRadius="5">
                {badgeText}
            </Badge>
        )}
    </Box>
);
