import React, { FC } from "react";
import { Text } from "@chakra-ui/react";

export const BasicText: FC<{ children: React.ReactNode }> = ({ children }) => {
    return <Text fontSize="sm">{children}</Text>;
};
