export const dataCode = `export const dataArray = [
    [
        {
            id: 1,
            name: "Harry Potter",
            city: "London",
        },
        {
            id: 2,
            name: "Don Quixote",
            city: "Madrid",
        },
        {
            id: 3,
            name: "Joan of Arc",
            city: "Paris",
        },
        {
            id: 4,
            name: "Rosa Park",
            city: "Alabama",
        },
    ],
];

export const dataObject = {
    data: {
            id: 1,
            name: "Harry Potter",
            city: "London",
        },
        {
            id: 2,
            name: "Don Quixote",
            city: "Madrid",
        },
        {
            id: 3,
            name: "Joan of Arc",
            city: "Paris",
        },
        {
            id: 4,
            name: "Rosa Park",
            city: "Alabama",
        },
    }
}

`;
