import React from "react";
import { NextPage } from "next";
import CollapsibleItem from "../../CollapsibleItem";
import { Box, Heading, Text } from "@chakra-ui/react";
import { PageWrapper } from "../../PageWrapper";
import { InfoPanel } from "../../InfoPanel";
import { Sandpack } from "@codesandbox/sandpack-react";
import { useSandpack } from "@codesandbox/sandpack-react";
import { autocompletion, completionKeymap } from "@codemirror/autocomplete";
import TaskTitle from "../TaskTitle";
import TaskDescription from "../TaskDescription";
import ResourcesLink from "../ResourcesLink";
import { assignmentMapFunctionData } from "../Javascript/data";
import AssignmentList from "../AssignmentList";
import { dataCode } from "./javascriptMapFiles";

const JavascriptTasks: NextPage = () => {
    const { sandpack } = useSandpack();
    const { files, activeFile } = sandpack;
    const code = files[activeFile].code;

    return (
        <PageWrapper>
            <Heading as="h3" size="md" mb="8">
                Javascript tasks
            </Heading>
            <Heading as="h5" size="xs" fontWeight="600" mb="4" color="gray">
                CSS part Javascript is an integral part of React development and
                helps us to display information according to our specifications
                and to process data that we have available locally and from the
                backend.
            </Heading>

            <Heading as="h3" size="sm" mb="8" color="gray">
                Lets try some excersice and show data
            </Heading>
            <Box my="40px">
                <Box px="1.2rem">
                    <CollapsibleItem isOpened title="Javascript map() function">
                        <TaskTitle taskTitle="Lets try map() function" />
                        {/* <InfoPanel text="this is a text" /> */}
                        <TaskDescription>
                            Its time to learn map function. Map function is
                            common javascript function used quite often. Keep
                            focus on side effects which can happened as - what
                            happened is you fetched data failed, why is
                            important key and what can be used for key or how
                            output looks like.
                        </TaskDescription>

                        <TaskDescription>
                            We are going to try map array and object as well
                        </TaskDescription>
                        <ResourcesLink href="https://medium.com/analytics-vidhya/learn-and-understand-javascripts-map-function-agbejule-kehinde-favour-ac91e3f61fff" />

                        <AssignmentList data={assignmentMapFunctionData} />

                        <Sandpack
                            template="react"
                            theme="dark"
                            files={{
                                "/App.js": {
                                    code: code,
                                    active: true,
                                },
                                "/data.js": {
                                    code: dataCode,
                                },
                            }}
                            options={{
                                recompileMode: "immediate",
                                showNavigator: false, // this will show a top navigator bar instead of the refresh button
                                showTabs: true, // you can toggle the tabs on/off manually
                                showLineNumbers: true, // this is off by default, but you can show line numbers for the editor
                                wrapContent: true, // also off by default, this wraps the code instead of creating horizontal overflow
                                editorWidthPercentage: 60, // by default the split is 50/50 between the editor and the preview
                                codeEditor: {
                                    extensions: [autocompletion()],
                                    extensionsKeymap: [completionKeymap],
                                },
                            }}
                        />
                    </CollapsibleItem>
                </Box>

                <Box px="1.2rem">
                    <CollapsibleItem
                        withoutMarginBottom
                        title="Javascript find() function"
                    >
                        <TaskTitle taskTitle="Lets try map() function" />
                        {/* <InfoPanel text="this is a text" /> */}
                        <TaskDescription>
                            Its time to learn map function. Map function is
                            common javascript function used quite often. Keep
                            focus on side effects which can happened as - what
                            happened is you fetched data failed, why is
                            important key and what can be used for key or how
                            output looks like.
                        </TaskDescription>

                        <TaskDescription>
                            How you handle initial data if you data will be
                            object and no array?
                        </TaskDescription>
                        <ResourcesLink href="https://medium.com/analytics-vidhya/learn-and-understand-javascripts-map-function-agbejule-kehinde-favour-ac91e3f61fff" />

                        <AssignmentList data={assignmentMapFunctionData} />

                        <Sandpack
                            template="react"
                            theme="dark"
                            files={{
                                "/App.js": {
                                    code: code,
                                    active: true,
                                },
                                "/data.js": {
                                    code: dataCode,
                                },
                            }}
                            options={{
                                recompileMode: "immediate",
                                showNavigator: false, // this will show a top navigator bar instead of the refresh button
                                showTabs: true, // you can toggle the tabs on/off manually
                                showLineNumbers: true, // this is off by default, but you can show line numbers for the editor
                                wrapContent: true, // also off by default, this wraps the code instead of creating horizontal overflow
                                editorWidthPercentage: 60, // by default the split is 50/50 between the editor and the preview
                                codeEditor: {
                                    extensions: [autocompletion()],
                                    extensionsKeymap: [completionKeymap],
                                },
                            }}
                        />
                    </CollapsibleItem>
                </Box>

                <Box px="1.2rem">
                    <CollapsibleItem
                        withoutMarginBottom
                        title="Javascript filter() function"
                    >
                        <TaskTitle taskTitle="Lets try map() function" />
                        {/* <InfoPanel text="this is a text" /> */}
                        <TaskDescription>
                            Its time to learn map function. Map function is
                            common javascript function used quite often. Keep
                            focus on side effects which can happened as - what
                            happened is you fetched data failed, why is
                            important key and what can be used for key or how
                            output looks like.
                        </TaskDescription>

                        <TaskDescription>
                            How you handle initial data if you data will be
                            object and no array?
                        </TaskDescription>
                        <ResourcesLink href="https://medium.com/analytics-vidhya/learn-and-understand-javascripts-map-function-agbejule-kehinde-favour-ac91e3f61fff" />

                        <AssignmentList data={assignmentMapFunctionData} />

                        <Sandpack
                            template="react"
                            theme="dark"
                            files={{
                                "/App.js": {
                                    code: code,
                                    active: true,
                                },
                                "/data.js": {
                                    code: dataCode,
                                },
                            }}
                            options={{
                                recompileMode: "immediate",
                                showNavigator: false, // this will show a top navigator bar instead of the refresh button
                                showTabs: true, // you can toggle the tabs on/off manually
                                showLineNumbers: true, // this is off by default, but you can show line numbers for the editor
                                wrapContent: true, // also off by default, this wraps the code instead of creating horizontal overflow
                                editorWidthPercentage: 60, // by default the split is 50/50 between the editor and the preview
                                codeEditor: {
                                    extensions: [autocompletion()],
                                    extensionsKeymap: [completionKeymap],
                                },
                            }}
                        />
                    </CollapsibleItem>
                </Box>

                <Box px="1.2rem">
                    <CollapsibleItem
                        withoutMarginBottom
                        title="Javascript reduce() function"
                    >
                        <TaskTitle taskTitle="Lets try map() function" />
                        {/* <InfoPanel text="this is a text" /> */}
                        <TaskDescription>
                            Its time to learn map function. Map function is
                            common javascript function used quite often. Keep
                            focus on side effects which can happened as - what
                            happened is you fetched data failed, why is
                            important key and what can be used for key or how
                            output looks like.
                        </TaskDescription>

                        <TaskDescription>
                            How you handle initial data if you data will be
                            object and no array?
                        </TaskDescription>
                        <ResourcesLink href="https://medium.com/analytics-vidhya/learn-and-understand-javascripts-map-function-agbejule-kehinde-favour-ac91e3f61fff" />

                        <AssignmentList data={assignmentMapFunctionData} />

                        <Sandpack
                            template="react"
                            theme="dark"
                            files={{
                                "/App.js": {
                                    code: code,
                                    active: true,
                                },
                                "/data.js": {
                                    code: dataCode,
                                },
                            }}
                            options={{
                                recompileMode: "immediate",
                                showNavigator: false, // this will show a top navigator bar instead of the refresh button
                                showTabs: true, // you can toggle the tabs on/off manually
                                showLineNumbers: true, // this is off by default, but you can show line numbers for the editor
                                wrapContent: true, // also off by default, this wraps the code instead of creating horizontal overflow
                                editorWidthPercentage: 60, // by default the split is 50/50 between the editor and the preview
                                codeEditor: {
                                    extensions: [autocompletion()],
                                    extensionsKeymap: [completionKeymap],
                                },
                            }}
                        />
                    </CollapsibleItem>
                </Box>
            </Box>
        </PageWrapper>
    );
};

export default JavascriptTasks;
