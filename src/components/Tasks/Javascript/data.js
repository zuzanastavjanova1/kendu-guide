import React from "react";

// TOOD: need to have task assignment in db - no mock

export const assignmentMapFunctionData = [
    {
        id: 1,
        text: (
            <>
                Create order list contain list of name from attached dataArray
                you can find in data.js
            </>
        ),
    },
    {
        id: 2,
        text: (
            <>
                Create order list contain list of name and city attached
                dataArray you can find in data.js. Expected output:{" "}
                <strong>I am Harry Potter from London</strong>
            </>
        ),
    },
    {
        id: 3,
        text: (
            <>
                Create unordered list contain list of name from attached
                dataObject you can find in data.js
            </>
        ),
    },
    {
        id: 4,
        text: (
            <>
                Create order list contain list of name and city attached
                dataObject you can find in data.js. Expected output:{" "}
                <strong>My name is Harry Potter and I am from London.</strong>
            </>
        ),
    },
];

const dataCode = `export const dataArray = [
    [
        {
            id: 1,
            name: "Harry Potter",
            city: "London",
        },
        {
            id: 2,
            name: "Don Quixote",
            city: "Madrid",
        },
        {
            id: 3,
            name: "Joan of Arc",
            city: "Paris",
        },
        {
            id: 4,
            name: "Rosa Park",
            city: "Alabama",
        },
    ],
];

export const dataObject = {
    data: {
            id: 1,
            name: "Harry Potter",
            city: "London",
        },
        {
            id: 2,
            name: "Don Quixote",
            city: "Madrid",
        },
        {
            id: 3,
            name: "Joan of Arc",
            city: "Paris",
        },
        {
            id: 4,
            name: "Rosa Park",
            city: "Alabama",
        },
    }
}

`;
