import React, { FC } from "react";
import { Text } from "@chakra-ui/react";

interface ITaskTitle {
    readonly children: string;
}

const TaskDescription: FC<ITaskTitle> = ({ children }) => (
    <Text my="1.6rem">{children}</Text>
);

export default TaskDescription;
