import React, { FC } from "react";
import { Link } from "@chakra-ui/react";
import { ExternalLinkIcon } from "@chakra-ui/icons";

interface ITaskTitle {
    readonly href: string;
}

const ResourcesLink: FC<ITaskTitle> = ({ href }) => {
    return (
        <Link
            href={href}
            isExternal
            color="brand.lila"
            fontWeight="700"
            fontSize="1rem"
            display="flex"
            alignItems="center"
        >
            Resources tip <ExternalLinkIcon mx="0.8rem" />
        </Link>
    );
};

export default ResourcesLink;
