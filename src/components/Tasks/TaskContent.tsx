import React from "react";
import JavascriptTasks from "./Javascript";
import { useRouter } from "next/router";
import HtmlAndCssTasks from "./HtmlAndCss";

function TaskContent() {
    const router = useRouter();
    const { slug } = router.query;
    const sectionFromSlug = slug?.toString() ?? null;

    const getSecondImage = (sectionFromSlug: string | null) => {
        if (!sectionFromSlug) {
            return null;
        }

        switch (sectionFromSlug) {
            case "javascript":
                return <JavascriptTasks />;
            case "htmlandcss":
                return <HtmlAndCssTasks />;
            default:
                return null;
        }
    };
    return <div>{getSecondImage(sectionFromSlug)}</div>;
}

export default TaskContent;
