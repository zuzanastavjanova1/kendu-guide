import React, { FC } from "react";
import { Text, UnorderedList, ListItem } from "@chakra-ui/react";

interface AssignMentItem {
    readonly id: number;
    readonly text: React.ReactNode;
}

interface IAssignmentList {
    readonly data: AssignMentItem[];
}

const AssignmentList: FC<IAssignmentList> = ({ data }) => {
    return (
        <div>
            <Text fontWeight="700" fontSize="1.3rem" my="1.6rem">
                Assignment
            </Text>
            <UnorderedList mb="2rem">
                {data.map((assignment: AssignMentItem) => (
                    <ListItem key={assignment.id} mb="0.6rem">
                        {assignment.text}
                    </ListItem>
                ))}
            </UnorderedList>
        </div>
    );
};

export default AssignmentList;
