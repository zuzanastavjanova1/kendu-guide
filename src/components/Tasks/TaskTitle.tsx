import React, { FC } from "react";
import { Text } from "@chakra-ui/react";

interface ITaskTitle {
    readonly taskTitle: string;
}

const TaskTitle: FC<ITaskTitle> = ({ taskTitle }) => (
    <Text fontSize="1.3rem" mb="1.6rem" color="brand.lila" fontWeight="700">
        {taskTitle}
    </Text>
);

export default TaskTitle;
