import { Box, Button, Center, Heading, HStack, Text } from "@chakra-ui/react";
import React from "react";

type FormType = "logIn" | "signUp";

interface IGetInBox {
    readonly type: FormType;
    readonly onClick: () => void;
    readonly title: string | React.ReactNode;
    readonly subTitle?: string | React.ReactNode;
    readonly form: React.ReactNode;
    readonly accountQuestionText: string | React.ReactNode;
    readonly buttonText: string | React.ReactNode;
}

const GetInBox: React.FC<IGetInBox> = ({
    onClick,
    title,
    subTitle,
    form,
    accountQuestionText,
    buttonText,
}) => {
    return (
        <Box>
            <Heading as="h2" size="xl" mb="8">
                {title}
            </Heading>
            <Text fontSize="lg" align="center" color="gray.500" mb="4">
                {subTitle}
            </Text>
            {form}
            <Center>
                <HStack spacing="2">
                    <Text fontSize="md">{accountQuestionText}</Text>
                    <Button variant="ghost" onClick={onClick}>
                        {buttonText}
                    </Button>
                </HStack>
            </Center>
        </Box>
    );
};

export default GetInBox;
