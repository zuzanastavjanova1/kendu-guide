import {
    Input,
    FormLabel,
    FormControl,
    FormErrorMessage,
} from "@chakra-ui/react";
import React, { FC } from "react";

interface IEmailInput {
    readonly emailError: string | null;
}

const EmailInput: FC<IEmailInput> = ({ emailError }) => {
    return (
        <FormControl isRequired>
            <FormLabel htmlFor="email">Email</FormLabel>
            <Input
                id="email"
                type="email"
                placeholder="yourEmailName@email.com"
                size="md"
                name="email"
            />
            {emailError && (
                <FormErrorMessage>Email is required.</FormErrorMessage>
            )}
        </FormControl>
    );
};

export default EmailInput;
