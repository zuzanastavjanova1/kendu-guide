import {
    Button,
    Input,
    InputGroup,
    InputRightElement,
    FormLabel,
    FormControl,
    FormErrorMessage,
} from "@chakra-ui/react";
import React, { FC } from "react";

interface IPasswordInput {
    readonly passwordError: string | null;
}

const PasswordInput: FC<IPasswordInput> = ({ passwordError }) => {
    const [show, setShow] = React.useState(false);
    const handleClick = () => setShow(!show);

    return (
        <FormControl isRequired>
            <FormLabel htmlFor="password">Password</FormLabel>
            <InputGroup size="md">
                <Input
                    pr="4.5rem"
                    type={show ? "text" : "password"}
                    placeholder="Enter password"
                    name="password"
                />
                <InputRightElement width="4.5rem">
                    <Button h="1.75rem" size="sm" onClick={handleClick}>
                        {show ? "Hide" : "Show"}
                    </Button>
                </InputRightElement>
            </InputGroup>
            {passwordError && (
                <FormErrorMessage>
                    I am sorry, but bad password :(
                </FormErrorMessage>
            )}
        </FormControl>
    );
};

export default PasswordInput;
