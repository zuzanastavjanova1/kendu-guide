import { Tag } from "@chakra-ui/react";
import React, { useState, FC } from "react";
import { useUpdateUsersResourcesItem } from "../../services/data/useUpdateUsersResourcesItem";
import { ResourcesFeatures } from "../Resources/constants";
import { IResource } from "../Resources/types";
import { IKeyword } from "./types";

interface IKeywordsProps extends IResource {
    readonly keyword: IKeyword;
}

export const Keyword: FC<IKeywordsProps> = ({
    isItemSelected,
    keyword,
    userId,
}) => {
    const [checked, setChecked] = useState(isItemSelected);

    const { mutation } = useUpdateUsersResourcesItem({
        userId,
        itemId: keyword.id,
        resources: ResourcesFeatures.KEYWORDS,
    });

    return (
        <>
            <Tag
                py="2"
                px="4"
                key={keyword.id}
                onClick={async () => {
                    await mutation.mutate();
                    await setChecked(!checked);
                }}
                color="black"
                border="1px solid"
                borderColor={checked ? "white" : "brand.yellow"}
                bg={checked ? "brand.yellow" : "transparent"}
                _hover={{
                    cursor: "pointer",
                }}
            >
                {keyword.word}
            </Tag>
        </>
    );
};
