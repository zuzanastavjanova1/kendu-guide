export interface IKeyword {
    readonly id: string;
    readonly level: string;
    readonly section: string;
    readonly word: string;
}
