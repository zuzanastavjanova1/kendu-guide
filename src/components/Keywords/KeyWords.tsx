import React, { FC } from "react";
import { Keyword } from "./Keyword";
import { useResourcesData } from "../../services/data/useResourcesData";
import useUsersSavedIds from "../../services/useUsersSavedIds";
import { useFilteredResultsData } from "../../hooks/useFilteredResultsData";
import { IResources } from "../Resources/types";
import ResourcesComponentContainer from "../Resources/ResourcesComponentContainer";
import { Wrap } from "@chakra-ui/react";
import { Card } from "../Card";
import { useIsIdAmongUsersIds } from "../../hooks/userIsIdAmongUserIds";
import { ResourcesFeatures } from "../Resources/constants";

export const KeyWords: FC<IResources> = ({
    filteredSection,
    sectionLevel,
    userId,
    section,
}) => {
    const userSavedIds = useUsersSavedIds(userId);
    const { data, isLoading, isError } = useResourcesData({
        filteredSection,
    });

    const allKeywords = data?.keywords;

    if (isLoading) {
        // TODO: create a loader
        return <div>...loading</div>;
    }

    if (!allKeywords || userSavedIds?.keywords == null || isError) {
        return null;
    }

    const resultsData = useFilteredResultsData({
        data: allKeywords,
        sectionLevel,
    });

    return (
        <ResourcesComponentContainer
            section={section}
            title={ResourcesFeatures.KEYWORDS}
            userSavedIds={userSavedIds?.keywords?.length}
            totalNumberOfItems={Object.entries(allKeywords).length}
        >
            <Card>
                <Wrap spacing="3">
                    {resultsData?.map((keyword) => {
                        const isItemSelected = useIsIdAmongUsersIds({
                            data: userSavedIds?.keywords,
                            id: keyword.id,
                        });

                        return (
                            <Keyword
                                isItemSelected={isItemSelected}
                                keyword={keyword}
                                key={keyword.id}
                                userId={userId}
                            />
                        );
                    })}
                </Wrap>
            </Card>
        </ResourcesComponentContainer>
    );
};
