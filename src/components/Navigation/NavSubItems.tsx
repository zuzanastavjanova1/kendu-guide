import React, { FC } from "react";
import { Box } from "@chakra-ui/react";
import { NavSubItem } from "./NavSubItem";
import { INavSubItems } from "./types";

export const NavSubItems: FC<INavSubItems> = ({
    nav,
    isNavOpen,
    animation,
}) => {
    return (
        <Box animation={animation}>
            {nav?.subLabels?.map((label) => {
                const linkPath = () => {
                    const topic = nav.text;
                    const subTopic = label.link;
                    return `/${topic.toLowerCase()}/${subTopic.toLowerCase()}`;
                };

                return (
                    <NavSubItem
                        link={linkPath()}
                        isNavOpen={isNavOpen}
                        key={label.text}
                        label={label}
                    />
                );
            })}
        </Box>
    );
};
