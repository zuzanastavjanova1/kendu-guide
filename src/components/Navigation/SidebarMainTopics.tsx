import React, { FC } from "react";
import { navigationData } from "./navigationData";
import { NavItem } from "./NavItem";
import { NavLink } from "./NavigationLink";
import { ISidebarMainTopics } from "./types";

const SidebarMainTopics: FC<ISidebarMainTopics> = ({
    isNavOpen,
    handleQuestionClick,
    openedItem,
}) => (
    <>
        {navigationData.map((nav) => (
            <NavLink link={nav.link} key={nav.id}>
                <NavItem
                    nav={nav}
                    isNavOpen={isNavOpen}
                    onClick={() => handleQuestionClick(nav.id)}
                    opened={openedItem === nav.id}
                />
            </NavLink>
        ))}
    </>
);

export default SidebarMainTopics;
