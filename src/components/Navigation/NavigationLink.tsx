import React, { FC } from "react";
import Link from "next/link";
import { INavLink } from "./types";

export const NavLink: FC<INavLink> = ({ link, children }) => {
    if (link) {
        return (
            <Link href={link} as={link} passHref>
                {children}
            </Link>
        );
    }
    return <>{children}</>;
};
