import React, { FC } from "react";
import {
    Box,
    Avatar,
    IconButton,
    HStack,
    Menu,
    MenuList,
    MenuButton,
    useDisclosure,
    Text,
    keyframes,
} from "@chakra-ui/react";

import Link from "next/link";
import { BiBell, BiLogOutCircle, BiUserCircle } from "react-icons/bi";
import { personalNavigationData } from "./navigationData";
import { NavMenuItems } from "./NavMenuItems";
import { NavMenuItem } from "./NavMenuItem";
import { signOut } from "next-auth/react";
import { IHeader } from "./types";

const visibility = keyframes`
  from {opacity: 0}
  to {opacity: 1}
`;

export const Header: FC<IHeader> = ({ isNavOpen }) => {
    const { isOpen, onOpen, onClose } = useDisclosure();

    const visibilityAnimation = `${visibility} 1s linear`;

    return (
        <Box
            width="100%"
            position="fixed"
            height="60px"
            bg="white"
            zIndex={2}
            display="flex"
            alignItems="center"
            justifyContent="flex-end"
        >
            <Box justifyContent="flex-end" display="flex">
                <HStack
                    spacing="24px"
                    mr={isNavOpen ? "100px" : "0px"}
                    transition="all 0.5s ease"
                >
                    <IconButton
                        as={BiBell}
                        aria-label="bell"
                        size="xs"
                        bg="white"
                        color="silver"
                        _hover={{
                            bg: "white",
                            cursor: "pointer",
                        }}
                    />
                    <Menu isOpen={isOpen}>
                        <MenuButton
                            aria-label="Options"
                            onMouseEnter={onOpen}
                            onMouseLeave={onClose}
                        >
                            <Avatar
                                name="Dan Abrahmov"
                                size="sm"
                                src="https://bit.ly/dan-abramov"
                            />
                        </MenuButton>
                        <MenuList onMouseEnter={onOpen} onMouseLeave={onClose}>
                            <Link href="/profile">
                                <NavMenuItem
                                    icon={BiUserCircle}
                                    ariaLabel="profile"
                                >
                                    Profile
                                </NavMenuItem>
                            </Link>
                            {personalNavigationData.map((data) => (
                                <NavMenuItems data={data} key={data.id} />
                            ))}
                            <NavMenuItem
                                icon={BiLogOutCircle}
                                ariaLabel="logout"
                                onClick={() =>
                                    signOut({
                                        callbackUrl: "/",
                                    })
                                }
                            >
                                Log out
                            </NavMenuItem>
                        </MenuList>
                    </Menu>
                </HStack>
                <Box
                    display="flex"
                    alignItems="center"
                    mr={isNavOpen ? "45px" : undefined}
                    animation={isNavOpen ? visibilityAnimation : undefined}
                >
                    <Text
                        color={isNavOpen ? "silver" : "white"}
                        opacity={isNavOpen ? "1" : "0"}
                    >
                        Progress bar
                    </Text>
                </Box>
            </Box>
        </Box>
    );
};
