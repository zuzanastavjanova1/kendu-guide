import { IconType } from "react-icons/lib";

export interface INavLink {
    readonly children: React.ReactNode;
    readonly link?: string;
}

export interface IArrowIconOpenNav {
    readonly setIsNavOpen: (isNavOpen: boolean) => void;
    readonly isNavOpen: boolean;
    readonly rightSidebar?: boolean;
}

export interface IHeader {
    readonly isNavOpen: boolean;
}

export interface ISubLabels {
    readonly id: number;
    readonly text: string;
    readonly link: string;
    readonly icon?: IconType;
}

export interface INavData {
    readonly id: number;
    readonly icon?: IconType;
    readonly text: string;
    readonly link?: string;
    readonly subLabels?: ISubLabels[];
}

export interface INavItem {
    readonly nav: INavData;
    readonly isNavOpen: boolean;
    readonly opened: boolean;
    readonly onClick?: () => void;
}

export interface INavMenuItem {
    readonly children: React.ReactNode;
    readonly icon?: IconType;
    readonly ariaLabel?: string;
    readonly onClick?: () => void;
}

export interface IGoalInput {
    readonly userGoal: string | null;
    readonly userId: string;
}

export interface ISidebarPersonalTopics {
    readonly isNavOpen: boolean;
    readonly handleQuestionClick: (data: number) => void;
    readonly openedItem: number | null;
}

export interface ISidebarMainTopics {
    readonly isNavOpen: boolean;
    readonly handleQuestionClick: (data: number) => void;
    readonly openedItem: number | null;
}

export interface ISidebar {
    readonly isNavOpen: boolean;
    readonly leftSidebar?: boolean;
    readonly topChildren: React.ReactNode;
    readonly bottomChildren?: React.ReactNode;
}

export interface INavTooltip {
    readonly isNavOpen: boolean;
    readonly nav: INavData;
    readonly children: React.ReactNode;
}

export interface INavSubItems {
    readonly nav: INavData;
    readonly isNavOpen: boolean;
    readonly animation: string | undefined;
}

export interface INavSubItem {
    readonly label: ISubLabels;
    readonly isNavOpen: boolean;
    readonly link: string;
}

export interface INavMenuItems {
    readonly data: INavData;
    readonly section?: string;
}
