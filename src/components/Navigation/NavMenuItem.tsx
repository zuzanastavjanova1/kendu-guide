import React, { FC } from "react";
import { MenuItem, Icon } from "@chakra-ui/react";
import { BiArrowFromLeft } from "react-icons/bi";
import { INavMenuItem } from "./types";

export const NavMenuItem: FC<INavMenuItem> = ({
    children,
    icon,
    ariaLabel,
    onClick,
}) => (
    <MenuItem
        className="nav"
        color="silver"
        onClick={onClick}
        _hover={{
            color: "black",
        }}
        icon={
            <Icon
                w={4}
                h={4}
                display="flex"
                as={icon ?? BiArrowFromLeft}
                aria-label={ariaLabel}
                color="silver"
                sx={{
                    ".nav:hover &": {
                        color: "black",
                    },
                }}
            />
        }
    >
        {children}
    </MenuItem>
);
