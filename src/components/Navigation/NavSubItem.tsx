import React, { FC } from "react";
import { Box, Text, useBoolean } from "@chakra-ui/react";
import { IconButton } from "@chakra-ui/react";
import { BiChevronRight } from "react-icons/bi";
import Link from "next/link";
import { INavSubItem } from "./types";

export const NavSubItem: FC<INavSubItem> = ({ label, isNavOpen, link }) => {
    const [isHoverSubLabel, setIsHoverSubLabel] = useBoolean();
    return (
        <Link href={link}>
            <Box
                pl="50px"
                mr="10px"
                fontSize="1.2rem"
                py="2"
                color="silver"
                _hover={{
                    color: "black",
                    cursor: "pointer",
                }}
                width="100%"
                justifyContent="space-between"
                onMouseEnter={setIsHoverSubLabel.on}
                onMouseLeave={setIsHoverSubLabel.off}
                transition={isNavOpen ? "all 0.9s ease" : "all 0.1s ease"}
                display={isNavOpen ? "flex" : "none"}
            >
                <Text>{label.text}</Text>
                <IconButton
                    as={BiChevronRight}
                    aria-label="open-subLabels"
                    size="sm"
                    bg="white"
                    width="5"
                    height="5"
                    color={isHoverSubLabel ? "black" : "silver"}
                />
            </Box>
        </Link>
    );
};
