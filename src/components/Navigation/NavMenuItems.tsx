import React, { FC } from "react";
import { NavMenuItem } from "./NavMenuItem";
import Link from "next/link";
import { INavMenuItems } from "./types";

export const NavMenuItems: FC<INavMenuItems> = ({ data, section }) => {
    const linkPath = () => {
        const subTopic = data.link;
        return section
            ? `/${section?.toLowerCase()}/${subTopic?.toLowerCase()}`
            : `/${subTopic?.toLowerCase()}`;
    };

    return (
        <Link href={linkPath()} key={data.id} as={linkPath()}>
            <NavMenuItem icon={data.icon} ariaLabel={data.text}>
                {data.text}
            </NavMenuItem>
        </Link>
    );
};
