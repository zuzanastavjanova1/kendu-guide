import React, { FC } from "react";
import { Box, List } from "@chakra-ui/react";
import { ISidebar } from "./types";

export const Sidebar: FC<ISidebar> = ({
    isNavOpen,
    leftSidebar,
    topChildren,
    bottomChildren,
}) => {
    return (
        <List
            w="56px"
            spacing={3}
            bg="white"
            height="100vh"
            boxShadow="5px 0 5px -5px rgb(0 0 0 / 0.1)"
            transition="all 0.5s ease"
            width={isNavOpen ? "12rem" : "3.5rem"}
            pt="14"
            top={0}
            position="fixed"
            display="grid"
            alignContent="space-between"
            right={!leftSidebar ? "0" : "undefined"}
        >
            <Box
                width={isNavOpen ? "12rem" : "3.5rem"}
                transition="all 0.5s ease"
            >
                {topChildren}
            </Box>

            <Box
                mb={6}
                width={isNavOpen ? "12rem" : "3.5rem"}
                transition="all 0.5s ease"
            >
                {bottomChildren}
            </Box>
        </List>
    );
};
