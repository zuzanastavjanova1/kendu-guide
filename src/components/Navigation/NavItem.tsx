import React, { FC } from "react";
import {
    Box,
    Text,
    ListItem,
    ListIcon,
    useBoolean,
    Collapse,
    useDisclosure,
    Menu,
    MenuButton,
    MenuList,
    keyframes,
} from "@chakra-ui/react";
import { IconButton } from "@chakra-ui/react";
import { BiChevronDown } from "react-icons/bi";
import { NavTooltip } from "./NavTooltip";

import { NavSubItems } from "./NavSubItems";
import { NavMenuItems } from "./NavMenuItems";
import { INavItem, ISubLabels } from "./types";

const visibility = keyframes`
  from {opacity: 0}
  to {opacity: 1}
`;

export const NavItem: FC<INavItem> = ({ nav, isNavOpen, opened, onClick }) => {
    const [isHover, setIsHover] = useBoolean();
    const { isOpen, onOpen, onClose } = useDisclosure();
    const isSidebarClose = isHover && nav?.subLabels?.length && !isNavOpen;
    const visibilityAnimation = `${visibility} 1s linear`;
    return (
        <>
            <NavTooltip isNavOpen={isNavOpen} nav={nav}>
                <ListItem
                    onMouseEnter={setIsHover.on}
                    onMouseLeave={setIsHover.off}
                    key={nav.id}
                    display="flex"
                    alignItems="center"
                    p="5"
                    h="10"
                    bg={opened || isHover ? "#F2F1F2" : "white"}
                    _hover={{
                        cursor: "pointer",
                    }}
                    onClick={onClick}
                >
                    <Menu isOpen={isOpen}>
                        <MenuButton
                            aria-label="Courses"
                            fontWeight="normal"
                            onMouseEnter={onOpen}
                            onMouseLeave={onClose}
                        >
                            <ListIcon
                                as={nav.icon}
                                color={isHover ? "black" : "silver"}
                                width="5"
                                height="5"
                            />
                        </MenuButton>
                        {isSidebarClose && (
                            <MenuList
                                onMouseEnter={onOpen}
                                onMouseLeave={onClose}
                            >
                                {nav?.subLabels.map((label: ISubLabels) => {
                                    return (
                                        <NavMenuItems
                                            data={label}
                                            key={label.id}
                                            section={nav.text}
                                        />
                                    );
                                })}
                            </MenuList>
                        )}
                    </Menu>

                    <Box
                        display="flex"
                        justifyContent="space-between"
                        width="100%"
                        animation={isNavOpen ? visibilityAnimation : undefined}
                    >
                        <Text
                            fontSize="1.2rem"
                            fontWeight="400"
                            transition={
                                isNavOpen ? "all 0.5s ease" : "all 0.1s ease"
                            }
                            opacity={isNavOpen ? "1" : "0"}
                            color={isHover ? "black" : "silver"}
                        >
                            {nav.text}
                        </Text>
                        {nav.subLabels && (
                            <IconButton
                                as={BiChevronDown}
                                aria-label="open-subLabels"
                                size="sm"
                                bg="white"
                                width="5"
                                height="5"
                                transition={
                                    isNavOpen
                                        ? "all 0.9s ease"
                                        : "all 0.1s ease"
                                }
                                opacity={isNavOpen ? "1" : "0"}
                                color={isHover ? "black" : "silver"}
                                _hover={{
                                    bg: "#F2F1F2",
                                    cursor: "pointer",
                                }}
                            />
                        )}
                    </Box>
                </ListItem>
            </NavTooltip>
            <Collapse in={opened} animateOpacity>
                <NavSubItems
                    nav={nav}
                    isNavOpen={isNavOpen}
                    animation={isNavOpen ? visibilityAnimation : undefined}
                />
            </Collapse>
        </>
    );
};
