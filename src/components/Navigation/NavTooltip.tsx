import { Tooltip } from "@chakra-ui/react";
import React, { FC } from "react";
import { INavTooltip } from "./types";

export const NavTooltip: FC<INavTooltip> = ({ isNavOpen, nav, children }) => {
    if (!isNavOpen) {
        return (
            <Tooltip
                label={nav.text}
                aria-label={nav.text}
                bg="black"
                color="white"
                placement="right-end"
            >
                {children}
            </Tooltip>
        );
    }
    return <>{children}</>;
};
