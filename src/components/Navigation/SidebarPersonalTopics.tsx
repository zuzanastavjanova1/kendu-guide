import React, { FC } from "react";
import { personalNavigationData } from "./navigationData";
import { NavItem } from "./NavItem";
import { NavLink } from "./NavigationLink";
import { ISidebarPersonalTopics } from "./types";

const SidebarPersonalTopics: FC<ISidebarPersonalTopics> = ({
    isNavOpen,
    handleQuestionClick,
    openedItem,
}) => (
    <>
        {personalNavigationData.map((nav) => (
            <NavLink link={nav.link} key={nav.id}>
                <NavItem
                    nav={nav}
                    isNavOpen={isNavOpen}
                    onClick={() => handleQuestionClick(nav.id)}
                    opened={openedItem === nav.id}
                />
            </NavLink>
        ))}
    </>
);

export default SidebarPersonalTopics;
