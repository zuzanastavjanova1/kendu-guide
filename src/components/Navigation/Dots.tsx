import { List, ListItem } from "@chakra-ui/react";
import React, { FC } from "react";

const dotsStyles = {
    width: 3,
    height: 3,
    borderRadius: "50%",
};

export const Dots: FC = () => (
    <List zIndex={3} display="flex" my="6">
        <ListItem {...dotsStyles} bg="brand.lila" ml={3} mr={3}></ListItem>
        <ListItem {...dotsStyles} bg="brand.green" mr={3}></ListItem>
        <ListItem {...dotsStyles} bg="brand.yellow"></ListItem>
    </List>
);
