import React, { FC } from "react";
import { IconButton } from "@chakra-ui/react";
import { IoIosArrowDropleft } from "react-icons/io";
import { IArrowIconOpenNav } from "./types";

export const ArrowIconOpenNav: FC<IArrowIconOpenNav> = ({
    setIsNavOpen,
    isNavOpen,
    rightSidebar,
}) => {
    const handleClick = () => setIsNavOpen(!isNavOpen);
    return (
        <IconButton
            color="silver"
            aria-label="nav"
            position="fixed"
            zIndex="3"
            as={IoIosArrowDropleft}
            onClick={() => handleClick()}
            height="26px"
            bg="white"
            top="18px"
            transition="all 0.5s ease"
            left={!rightSidebar ? (isNavOpen ? "17rem" : "1rem") : "undefined"}
            right={rightSidebar ? (isNavOpen ? "17rem" : "1rem") : "undefined"}
            outline="none"
            borderRadius="50%"
            _active={{
                bg: "white",
            }}
            _hover={{
                bg: "white",
                cursor: "pointer",
            }}
            transform={
                rightSidebar
                    ? isNavOpen
                        ? "rotate(180deg)"
                        : "rotate(0deg)"
                    : isNavOpen
                    ? "rotate(0deg)"
                    : "rotate(180deg)"
            }
        />
    );
};
