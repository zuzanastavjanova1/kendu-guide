import {
    BiHomeAlt,
    BiRocket,
    BiDna,
    BiAtom,
    BiFileFind,
    BiBookmark,
    BiHeart,
} from "react-icons/bi";
import { ResourcesLevel, Sections } from "../Resources/constants";
import { INavData } from "./types";

export const navigationData: INavData[] = [
    {
        id: 1,
        icon: BiHomeAlt,
        text: "Home",
        link: "/",
    },
    {
        id: 2,
        icon: BiFileFind,
        text: "Resources",
        subLabels: [
            { id: 1, text: "html", link: "html" },
            { id: 2, text: "css", link: Sections.CSS },
            { id: 3, text: "javascript", link: Sections.JAVASCRIPT },
            { id: 4, text: "git", link: Sections.GIT },
            { id: 5, text: "react", link: Sections.REACT },
        ],
    },
    {
        id: 3,
        icon: BiAtom,
        text: "Projects",
        subLabels: [
            { id: 1, text: "Html + Css", link: "htmlAndCss" },
            { id: 2, text: "react", link: "react" },
        ],
    },
    {
        id: 4,
        icon: BiDna,
        text: "Tasks",
        subLabels: [
            { id: 1, text: "Html + Css", link: "htmlAndCss" },
            { id: 2, text: "javascript", link: "javascript" },
            { id: 3, text: "react", link: "react" },
        ],
    },
    {
        id: 5,
        icon: BiRocket,
        text: "Tips",
        subLabels: [
            { id: 1, text: "html", link: "html" },
            { id: 2, text: "css", link: Sections.CSS },
            { id: 3, text: "javascript", link: Sections.JAVASCRIPT },
            { id: 4, text: "git", link: Sections.GIT },
            { id: 5, text: "react", link: Sections.REACT },
        ],
    },
];

export const personalNavigationData = [
    {
        id: 6,
        icon: BiHeart,
        text: "My Projects",
        link: "projects",
    },
    {
        id: 7,
        icon: BiBookmark,
        text: "Saved things",
        link: "profile",
    },
];

export const progressTrackingData = [
    {
        id: 1,
        icon: BiHomeAlt,
        text: "HTML",
        link: "/resources/html",
        levels: [
            {
                id: 1,
                text: "Basic",
                link: ResourcesLevel.BASIC,
                checkedStatus: true,
            },
            {
                id: 2,
                text: "Advance",
                link: ResourcesLevel.ADVANCE,
                checkedStatus: false,
            },
            {
                id: 3,
                text: "Pro",
                link: ResourcesLevel.PRO,
                checkedStatus: false,
            },
        ],
    },
    {
        id: 2,
        icon: BiHomeAlt,
        text: "CSS",
        link: "/resources/css",
        levels: [
            {
                id: 1,
                text: "Basic",
                link: ResourcesLevel.BASIC,
                checkedStatus: false,
            },
            {
                id: 2,
                text: "Advance",
                link: ResourcesLevel.ADVANCE,
                checkedStatus: false,
            },
            {
                id: 3,
                text: "Pro",
                link: ResourcesLevel.PRO,
                checkedStatus: false,
            },
        ],
    },
    {
        id: 3,
        icon: BiHomeAlt,
        text: "Git",
        link: "/resources/git",
        levels: [
            {
                id: 1,
                text: "Basic",
                link: ResourcesLevel.BASIC,
                checkedStatus: false,
            },
            {
                id: 2,
                text: "Advance",
                link: ResourcesLevel.ADVANCE,
                checkedStatus: false,
            },
            {
                id: 3,
                text: "Pro",
                link: ResourcesLevel.PRO,
                checkedStatus: false,
            },
        ],
    },
    {
        id: 4,
        icon: BiHomeAlt,
        text: "JS",
        link: "/resources/javascript",
        levels: [
            {
                id: 1,
                text: "Basic",
                link: ResourcesLevel.BASIC,
                checkedStatus: false,
            },
            {
                id: 2,
                text: "Advance",
                link: ResourcesLevel.ADVANCE,
                checkedStatus: false,
            },
            {
                id: 3,
                text: "Pro",
                link: ResourcesLevel.PRO,
                checkedStatus: false,
            },
        ],
    },
    {
        id: 5,
        icon: BiHomeAlt,
        text: "React",
        link: "/resources/react",
        levels: [
            {
                id: 1,
                text: "Basic",
                link: ResourcesLevel.BASIC,
                checkedStatus: false,
            },
            {
                id: 2,
                text: "Advance",
                link: ResourcesLevel.ADVANCE,
                checkedStatus: false,
            },
            {
                id: 3,
                text: "Pro",
                link: ResourcesLevel.PRO,
                checkedStatus: false,
            },
        ],
    },
];
