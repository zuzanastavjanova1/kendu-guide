import React, { FC } from "react";
import { Center, Heading, Box, Text } from "@chakra-ui/react";

interface ILoginContainerWrapper {
    readonly mainWelcomeText: string;
    readonly description: string;
    readonly children: React.ReactNode;
}

const LoginContainerWrapper: FC<ILoginContainerWrapper> = ({
    mainWelcomeText,
    description,
    children,
}) => {
    return (
        <Center height="100vh">
            <Box display="grid" alignItems="center">
                <Center>
                    <Heading as="h2" size="xl" mb="8">
                        {mainWelcomeText}
                    </Heading>
                </Center>
                <Text fontSize="lg" align="center" color="gray.500" mb="4">
                    {description}
                </Text>
                {children}
            </Box>
        </Center>
    );
};

export default LoginContainerWrapper;
