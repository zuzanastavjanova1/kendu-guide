import { Box, Text } from "@chakra-ui/react";
import React, { FunctionComponent, useEffect, useState } from "react";
import { IconButton } from "@chakra-ui/react";
import { BiChevronDown, BiChevronUp } from "react-icons/bi";

interface CollapsibleItemProps {
    readonly children: React.ReactNode | Array<React.ReactNode>;
    readonly title: string | React.ReactNode;
    readonly isOpened?: boolean;
    readonly onOpenedChange?: () => void;
    readonly withoutMarginBottom?: boolean;
}

const CollapsibleItem: FunctionComponent<CollapsibleItemProps> = ({
    title,
    isOpened,
    children = "",
    onOpenedChange,
    withoutMarginBottom,
}) => {
    const [opened, setOpened] = useState(isOpened);

    const handleButtonClick = () => {
        setOpened(!opened);
        if (onOpenedChange) {
            onOpenedChange();
        }
    };

    useEffect(() => {
        setOpened(isOpened);
    }, [isOpened]);

    return (
        <Box
            bg="white"
            alignItems="center"
            px="1rem"
            borderRadius="0.4rem"
            pb={opened ? "4rem" : undefined}
            mb={opened ? "4rem" : undefined}
        >
            <Box
                display="flex"
                alignItems="center"
                justifyContent="space-between"
                mb="1.6rem"
                borderBottom={opened ? "1px solid silver" : "undefined"}
            >
                <Text
                    lineHeight="2.4rem"
                    my="0.8rem"
                    fontSize="1.2rem"
                    letterSpacing="1px"
                >
                    {title}
                </Text>

                <IconButton
                    onClick={handleButtonClick}
                    as={opened ? BiChevronDown : BiChevronUp}
                    aria-label="open-subLabels"
                    size="sm"
                    bg="white"
                    width="5"
                    height="5"
                    _hover={{
                        bg: "#F2F1F2",
                        cursor: "pointer",
                    }}
                />
            </Box>

            {opened && children}
        </Box>
    );
};

export default CollapsibleItem;
