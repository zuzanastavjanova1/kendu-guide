import { extendTheme, type ThemeConfig } from "@chakra-ui/react";

const config: ThemeConfig = {
    initialColorMode: "light",
    useSystemColorMode: false,
};

export const theme = extendTheme({
    config,
    colors: {
        brand: {
            yellow: "#ECEB98",
            green: "#71D8B9",
            lila: "#5624D0",
            background: "#F7FAFC",
            gradient:
                "linear-gradient(90deg, #71d8b9, #81dcb2, #93e0ac, #a4e3a5, #b6e6a0, #c8e89c, #daea99, #eceb98);",
        },
    },
});
