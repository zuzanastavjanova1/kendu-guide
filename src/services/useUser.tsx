import { db } from "../../lib/firebase";
import { updateDoc, doc } from "firebase/firestore";

// update user name
export const updateName = ({
    userId,
    userName,
}: {
    userId: string;
    userName: string | null;
}) => {
    const userNameRef = doc(db, "users", userId);
    return updateDoc(userNameRef, {
        name: userName,
    });
};

// update user goal
export const updateMyGoal = ({
    userId,
    userGoal,
}: {
    userId: string;
    userGoal: string | null;
}) => {
    const goalRef = doc(db, "users", userId);
    return updateDoc(goalRef, {
        goal: userGoal,
    });
};

// update user topics
export const updateUserSavedTopics = ({
    userId,
    ids,
}: {
    userId: string;
    ids: string[];
}) => {
    const userNameRef = doc(db, "users", userId);
    return updateDoc(userNameRef, {
        topics: {
            id: ids,
        },
    });
};
