import React from "react";
import { db } from "../../lib/firebase";
import { doc } from "firebase/firestore";
import { useFirestoreDocument } from "@react-query-firebase/firestore";

interface IData {
    readonly articles: string[];
    readonly keywords: string[];
    readonly questions: string[];
    readonly email: string;
    readonly emailVerified: null;
    readonly image: string;
    readonly name: string;
}

const useUsersSavedIds = (userId: string): any | null => {
    const ref = doc(db, "users", userId);

    const users = useFirestoreDocument(["users", userId], ref);

    const snapshot = users.data;

    const data = snapshot?.data();

    if (!data) {
        return null;
    }

    return data;
};

export default useUsersSavedIds;
