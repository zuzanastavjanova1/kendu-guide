import { ILevels } from "../components/ProgressStepper/types";
import { useDataDependOnCollection } from "./data/useDataDependOnCollection";

interface ISection {
    readonly name: string;
    readonly shortName: string;
    readonly link: string;
    readonly levels: ILevels[];
}

export const useSections = (): ISection[] | null => {
    const sections = useDataDependOnCollection("sections");

    return sections;
};

// TODO remove - no needed
export const useSectionsName = () => {
    const sections = useSections();
    return sections?.map((section) => section.name);
};
