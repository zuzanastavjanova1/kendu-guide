export {};
// import { db } from "../../lib/firebase";
// import { useDataDependOnCollection } from "./data/useDataDependOnCollection";

// import { updateDoc, doc } from "firebase/firestore";
// import { useAuth } from "../../context/AuthUserContext";

// export interface IApis {
//     readonly id: string;
//     readonly name: string;
//     readonly link: string;
//     readonly description: string;
// }

// export const useAllApis = () => {
//     const apis = useDataDependOnCollection("apis");

//     return apis;
// };

// export const useApisIds = () => {
//     const apis = useAllApis();

//     return apis?.map((api) => api.id);
// };

// export const useUserApisQuestions = () => {
//     const apis = useAllApis();
//     const { userApis } = useAuth();

//     return apis?.filter((key: IApis) => userApis?.includes(key.id)) ?? [];
// };

// // update user apis list
// export const updateUserSavedApis = ({
//     userId,
//     ids,
// }: {
//     userId: string;
//     ids: string[];
// }) => {
//     const userNameRef = doc(db, "users", userId);
//     return updateDoc(userNameRef, {
//         apis: {
//             id: ids,
//         },
//     });
// };
