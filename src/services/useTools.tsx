export interface ITool {
    readonly description: string;
    readonly item: string;
    readonly link: string;
    readonly section: string;
    readonly tags: string[];
    readonly id: string;
}
