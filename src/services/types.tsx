export interface IUserData {
    readonly articles?: string[];
    readonly keywords?: string[];
    readonly questions?: string[];
    readonly email: string;
    readonly emailVerified: null;
    readonly image: string;
    readonly name: string;
}
