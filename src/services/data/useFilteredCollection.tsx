import { useFirestoreQuery } from "@react-query-firebase/firestore";
import { collection, query } from "firebase/firestore";
import { db } from "../../../lib/firebase";

// TODO fuck add return type
export const useFilteredCollection = ({
    nameCollection,
    filteredSection,
}: {
    nameCollection: string;
    filteredSection: string;
}) => {
    const ref = query(collection(db, nameCollection));
    const toolsQuery = useFirestoreQuery([nameCollection], ref, {
        subscribe: false,
    });

    const isLoading = toolsQuery.isLoading;
    const isError = toolsQuery.isError;
    const filteredData = toolsQuery.data?.docs.filter((i) => {
        if (i.data().section) {
            return i.data().section === filteredSection;
        }
        if (i.data().tags) {
            return i.data().tags.includes(filteredSection);
        }
    });

    const data = filteredData?.map((i) => ({
        id: i.id,
        ...i.data(),
    }));

    return { data, isLoading, isError };
};
