import { useFirestoreQuery } from "@react-query-firebase/firestore";
import { collection, query } from "firebase/firestore";
import { db } from "../../../lib/firebase";

// TODO  add return type
export const useFilteredCollectionWithLevel = ({
    nameCollection,
    filteredSection,
}: {
    nameCollection: string;
    filteredSection: string;
}) => {
    const ref = query(collection(db, nameCollection));

    const toolsQuery = useFirestoreQuery([nameCollection], ref, {
        subscribe: true,
    });

    const isLoading = toolsQuery.isLoading;
    const isError = toolsQuery.isError;
    const filteredData = toolsQuery.data?.docs.filter((i) => {
        if (i.data().articles) {
            return i.data().articles === filteredSection;
        }
        if (i.data().tags) {
            return i.data().tags.includes(filteredSection);
        }
    });

    const data = filteredData?.map((i) => ({
        id: i.id,
        ...i.data(),
    }));

    return { data, isLoading, isError };
};
