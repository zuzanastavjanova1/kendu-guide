import { useState, useEffect } from "react";
import { collection, query, onSnapshot } from "firebase/firestore";
import { db } from "../../../lib/firebase";

// mainly get data just for show
// TODO add return type and remove any type face palm
export const useDataDependOnCollection = (type: string) => {
    const [loading, setLoading] = useState<boolean>(true);
    const [data, setData] = useState<any[]>([]);

    useEffect(() => {
        const q = query(collection(db, type));

        onSnapshot(q, (querySnapshot) => {
            const data = querySnapshot.docs.map((doc) => doc.data());

            setData(data);
            setLoading(false);
        });
    }, []);

    if (loading) {
        return null;
    }

    return data;
};
