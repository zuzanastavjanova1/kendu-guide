import { useFirestoreQuery } from "@react-query-firebase/firestore";
import { collection, query } from "firebase/firestore";
import { db } from "../../../lib/firebase";

// TODO remove
export const useCollection = ({
    nameCollection,
    id,
}: {
    nameCollection: string;
    id: string;
}) => {
    const ref = query(collection(db, nameCollection));
    const toolsQuery = useFirestoreQuery([nameCollection], ref, {
        subscribe: false,
    });

    const isLoading = toolsQuery.isLoading;
    const isError = toolsQuery.isError;
    const data = toolsQuery.data?.docs.map((i) => {
        return {
            id: i.id,
            ...i.data(),
        };
    });

    if (data?.map((i) => i.id === id))
        return { data: data, isLoading, isError };
};
