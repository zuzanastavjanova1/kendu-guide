import { useState, useEffect } from "react";
import { collection, query, onSnapshot } from "firebase/firestore";
import { db } from "../../../lib/firebase";

// TODO add return type
export const useDataCollection = (type: string) => {
    const [loading, setLoading] = useState<boolean>(true);
    const [collectionData, setDataCollection] = useState<any[]>([]);

    useEffect(() => {
        const q = query(collection(db, type));

        onSnapshot(q, (querySnapshot) => {
            const allCollectionData = querySnapshot.docs.map((doc) => ({
                id: doc.id,
                ...doc.data(),
            }));
            setDataCollection(allCollectionData);
            setLoading(false);
        });
    }, []);

    if (loading) {
        return null;
    }

    return collectionData;
};
