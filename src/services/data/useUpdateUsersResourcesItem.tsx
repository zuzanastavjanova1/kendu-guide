import { useFirestoreTransaction } from "@react-query-firebase/firestore";
import { collection, doc } from "firebase/firestore";
import { db } from "../../../lib/firebase";

interface IUserResourcesItem {
    readonly itemId: string;
    readonly userId: string;
    readonly resources: string;
}

// TODO  add return type
export const useUpdateUsersResourcesItem = ({
    itemId,
    userId,
    resources,
}: IUserResourcesItem) => {
    const collections = collection(db, "users");
    const ref = doc(collections, userId);

    const mutation = useFirestoreTransaction(ref.firestore, async (tsx) => {
        const doc = await tsx.get(ref);

        const items = doc.data()?.[resources];

        const updatedItems = [...items, itemId];

        const findDuplicatesItemForRemove = updatedItems.filter(
            (item: string, index: number) =>
                updatedItems.indexOf(item) !== index
        );

        const cleanIdsList = updatedItems.filter(
            (id: string) => id !== findDuplicatesItemForRemove[0]
        );

        tsx.update(ref, {
            [resources]: cleanIdsList,
        });
    });

    return {
        mutation,
    };
};
