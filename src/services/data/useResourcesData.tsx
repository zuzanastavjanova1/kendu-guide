import { useFirestoreQuery } from "@react-query-firebase/firestore";
import { collection, query } from "firebase/firestore";
import { db } from "../../../lib/firebase";
import { Sections } from "../../components/Resources/constants";

// TODO  add return type
export const useResourcesData = ({
    filteredSection,
}: {
    filteredSection: Sections;
}) => {
    const ref = query(collection(db, "resources"));
    const toolsQuery = useFirestoreQuery(["resources"], ref, {
        subscribe: false,
    });

    const isLoading = toolsQuery.isLoading;
    const isError = toolsQuery.isError;

    const data = toolsQuery.data?.docs?.map((i) => {
        return i.data();
    });

    const resourcesData = data && data[0][filteredSection];

    return {
        data: resourcesData,
        isLoading,
        isError,
    };
};
