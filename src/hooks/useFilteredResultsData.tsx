import { IArticle } from "../components/Articles/types";

export const useFilteredResultsData = ({
    data,
    sectionLevel,
}: any): IArticle[] => {
    const filteredDataViaLevel = Object.entries(data).map((data) => {
        const value = data[1];
        // @ts-ignore
        const bla = value?.level === sectionLevel;

        if (bla) {
            return {
                id: data[0],
                // @ts-ignore
                ...value,
            };
        }
    });

    const resultsData = filteredDataViaLevel.filter((element) => {
        return element !== undefined;
    });

    return resultsData;
};
