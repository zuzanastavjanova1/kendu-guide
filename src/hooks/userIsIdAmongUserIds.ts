export const useIsIdAmongUsersIds = ({
    data,
    id,
}: {
    data: string[];
    id: string;
}) => {
    const isItemSelected = data?.includes(id);

    return isItemSelected;
};
