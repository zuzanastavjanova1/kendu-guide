export const useSelectUniqueIds = (newUsersSavedIds: any[]) => {
    const findDuplicatesItemForRemove = newUsersSavedIds.filter(
        (item: string, index: number) =>
            newUsersSavedIds.indexOf(item) !== index
    );

    const cleanIdsList = newUsersSavedIds.filter(
        (id: string) => id !== findDuplicatesItemForRemove[0]
    );

    return cleanIdsList;
};
