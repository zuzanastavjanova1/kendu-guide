import {
    BiDrink,
    BiBrain,
    BiPencil,
    BiTagAlt,
    BiPulse,
    BiAtom,
    BiPin,
    BiDetail,
} from "react-icons/bi";

export const necessaryThings = [
    { id: 1, text: "Editor", icon: BiPencil },
    {
        id: 2,
        text: "Github and Gitlab account - depend on you preferred",
        icon: BiTagAlt,
    },
    { id: 3, text: "Be curious", icon: BiDrink },
    { id: 4, text: "Be hardworking and not give up", icon: BiBrain },
];

export const topicsDivision = [
    {
        id: 1,
        title: "Resources",
        description:
            "Part of the resources is divided into topics and you can find materials and knowledge from a lot of inspiring people. There are links to articles, videos, courses and also tips on tools that will help you be more efficient at work and primarily make it easier. You will also find keywords part here, which are keywords that you should explain and therefore be familiar with. There are also questions that will help you, for example, in an interview and improve your orientation in the given issue. The aim is not to memorize the answers, but to understand the issue and better orient yourself in it.",
        icon: BiDetail,
    },
    {
        id: 3,
        title: "Projects",
        description:
            "In the projects section, you can find projects that can serve as an interview presentation. These assignments are intended to simulate a real developer's project and thus help to learn how to work effectively. See the detailed assignment for more information. The projects also include a review of projects, which is developed by developers. It serves to improve the project and as feedback from us on your work. Every developer meets with the reviews on a daily basis and should give reviews to other developers as well, so it's time to get to know him.",
        icon: BiAtom,
    },
    {
        id: 4,
        title: "Tasks",
        description:
            "In the task section of the guide, you will find various tasks that you can try. These tasks represent the real work of the developers and their assignment is described in detail. The goal is to try smaller tasks that work them out to perfection.",
        icon: BiPulse,
    },
    {
        id: 5,
        title: "Tips",
        description:
            "In the tips section you can find interesting tips that should make your work easier and inspired by various solutions. It's primarily for curious people, which developers should be. Do you have a great tip that is missing here? Let us know.",
        icon: BiPin,
    },
];
