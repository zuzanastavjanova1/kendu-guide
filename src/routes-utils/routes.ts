//home is only exception in routing that not corresponds with file instead of index is "/"
export enum Route {
    home = "/",
    resources = "/resources/[slug]",
    project = "/project/[id]",
    projects = "/projects/[slug]",
    tasks = "/tasks/[slug]",
    tips = "/tips/[slug]",
    login = "/login",
    profile = "/profile",
}

export const isExistingRoute = (pathname: string): pathname is Route =>
    Object.values(Route).includes(pathname as Route);
