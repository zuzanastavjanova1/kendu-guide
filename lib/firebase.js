import "firebase/compat/auth";
import "firebase/compat/firestore";

import { initializeApp, getApps, getApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
    apiKey: "AIzaSyD7Mc8oMU-r-lBJh2ikHyLUkcdD0fMAkU0",
    authDomain: "kenduguide-318fa.firebaseapp.com",
    databaseURL: "https://kenduguide-318fa-default-rtdb.firebaseio.com/",
    projectId: "kenduguide-318fa",
    storageBucket: "kenduguide-318fa.appspot.com",
    messagingSenderId: "483472666412",
    appId: "1:483472666412:web:c6e0bc1dcebfd620d1c689",
    measurementId: "G-Y8RSVD56PW",
};

const app = getApps.length > 0 ? getApp() : initializeApp(firebaseConfig);
const appp = initializeApp(firebaseConfig);

const db = getFirestore(app);
const storage = getStorage(app);

export { app, appp, db, storage };
