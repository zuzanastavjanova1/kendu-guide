import React from "react";
import { ColorModeScript } from "@chakra-ui/react";
import NextDocument, { Html, Head, Main, NextScript } from "next/document";
import { getSandpackCssText } from "@codesandbox/sandpack-react";
import { theme } from "../src/styles/ChakraTheme";

export default class Document extends NextDocument {
    render() {
        return (
            <Html lang="en">
                <Head>
                    <style
                        dangerouslySetInnerHTML={{
                            __html: getSandpackCssText(),
                        }}
                        id="sandpack"
                    />
                </Head>
                <body>
                    <ColorModeScript
                        initialColorMode={theme.config.initialColorMode}
                    />
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}
