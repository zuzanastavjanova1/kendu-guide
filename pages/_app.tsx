import React from "react";
import App, { AppProps } from "next/app";
import { QueryClient, QueryClientProvider } from "react-query";
import { ChakraProvider } from "@chakra-ui/react";
import { extendTheme } from "@chakra-ui/react";
import { theme } from "../src/styles/ChakraTheme";
import { SessionProvider } from "next-auth/react";
import { AuthUserProvider } from "../context/AuthUserContext";
import ms from "ms";
import Container from "../src/components/Container";
import Login from "./Login";
import { getSession } from "next-auth/react";
import { DefaultOptions } from "react-query";
import {
    SandpackProvider,
    SandpackThemeProvider,
} from "@codesandbox/sandpack-react";
import "@code-hike/mdx/dist/index.css";

import { User } from "next-auth/core/types";
import ErrorPage from "./_error";

const themes = extendTheme(theme);

interface Ids {
    readonly id: string[];
}

interface UserInfo extends User {
    readonly keywords?: Ids;
    readonly questions?: Ids;
    readonly videos?: Ids;
    readonly apis?: Ids;
    readonly articles?: Ids;
    readonly topics?: Ids;
    readonly goal?: string;
    readonly id: string;
}

interface Session {
    readonly expires: string;
    readonly user: UserInfo;
}

export interface QueryConfig {
    defaultOptions?: DefaultOptions;
}

export const QUERY_CLIENT_CONFIG: QueryConfig = {
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
            staleTime: ms("24h") as number,
            cacheTime: ms("5h") as number,
        },
    },
};

const queryClient = new QueryClient(QUERY_CLIENT_CONFIG);

const MyApp = ({
    Component,
    pageProps: { ...pageProps },
    session,
    statusCode,
}: AppProps & { session: Session | any; statusCode: number }) => {
    if (statusCode && statusCode !== 200) {
        return <ErrorPage statusCode={statusCode as number} />;
    }

    if (!session) {
        return (
            <ChakraProvider theme={themes}>
                <Login />;
            </ChakraProvider>
        );
    }

    const { user } = session;

    // users properties are created step by step - need to be changed types

    const userId = user.id;
    const userKeywords = user?.keywords?.id ?? [];
    const userQuestions = user?.questions?.id ?? [];
    const userTopics = user?.topics?.id ?? [];
    const userVideos = user?.videos?.id ?? [];
    const userApis = user?.apis?.id ?? [];
    const userArticles = user?.articles?.id ?? [];
    const userGoal = user?.goal ?? null;

    return (
        <SessionProvider session={session}>
            <AuthUserProvider
                authUser={session.user}
                userId={userId}
                userKeywords={userKeywords}
                userQuestions={userQuestions}
                userTopics={userTopics}
                userVideos={userVideos}
                userApis={userApis}
                userArticles={userArticles}
                userGoal={userGoal}
            >
                <SandpackProvider template="react">
                    <SandpackThemeProvider theme="dark">
                        <QueryClientProvider client={queryClient}>
                            <ChakraProvider theme={themes}>
                                <Container>
                                    <Component {...pageProps} />
                                </Container>
                            </ChakraProvider>
                        </QueryClientProvider>
                    </SandpackThemeProvider>
                </SandpackProvider>
            </AuthUserProvider>
        </SessionProvider>
    );
};

MyApp.getInitialProps = async (context) => {
    const appProps = await App.getInitialProps(context);
    const session = await getSession(context);

    return {
        ...appProps,
        statusCode: context.ctx.res?.statusCode,
        session,
    };
};

export default MyApp;
