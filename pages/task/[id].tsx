import React from "react";
import { NextPage } from "next";
import CollapsibleItem from "../../src/components/CollapsibleItem";
import { Box } from "@chakra-ui/react";
import { PageWrapper } from "../../src/components/PageWrapper";
import { Sandpack } from "@codesandbox/sandpack-react";
import { useSandpack } from "@codesandbox/sandpack-react";
import { autocompletion, completionKeymap } from "@codemirror/autocomplete";
import TaskTitle from "../../src/components/Tasks/TaskTitle";
import TaskDescription from "../../src/components/Tasks/TaskDescription";
import ResourcesLink from "../../src/components/Tasks/ResourcesLink";
import { assignmentMapFunctionData } from "../../src/components/Tasks/Javascript/data";
import AssignmentList from "../../src/components/Tasks/AssignmentList";
import { dataCode } from "../../src/components/Tasks/Javascript/javascriptMapFiles";

// TODO FUCK routing system via structure - no numbers as a ID in route
// TODO need to create some normal component - this is just mock for demo - falcePalm

const Task: NextPage = () => {
    const { sandpack } = useSandpack();
    const { files, activeFile } = sandpack;
    const code = files[activeFile].code;

    return (
        <PageWrapper>
            <Box my="4rem">
                <Box px="1.2rem">
                    <CollapsibleItem title="Javascript map() function">
                        <TaskTitle taskTitle="Lets try map() function" />
                        {/* <InfoPanel text="this is a text" /> */}
                        <TaskDescription>
                            Its time to learn map function. Map function is
                            common javascript function used quite often. Keep
                            focus on side effects which can happened as - what
                            happened is you fetched data failed, why is
                            important key and what can be used for key or how
                            output looks like.
                        </TaskDescription>

                        <TaskDescription>
                            How you handle data if you data will be object and
                            no array?
                        </TaskDescription>
                        <ResourcesLink href="https://medium.com/analytics-vidhya/learn-and-understand-javascripts-map-function-agbejule-kehinde-favour-ac91e3f61fff" />

                        <AssignmentList data={assignmentMapFunctionData} />

                        <Sandpack
                            template="react"
                            theme="dark"
                            files={{
                                "/App.js": {
                                    code: code,
                                    active: true,
                                },
                                "/data.js": {
                                    code: dataCode,
                                },
                            }}
                            options={{
                                recompileMode: "immediate",
                                showNavigator: false, // FMI this will show a top navigator bar instead of the refresh button
                                showTabs: true, // FMI you can toggle the tabs on/off manually
                                showLineNumbers: true, // this is off by default, but you can show line numbers for the editor
                                wrapContent: true, // also off by default, this wraps the code instead of creating horizontal overflow
                                editorWidthPercentage: 70, // by default the split is 70/30 between the editor and the preview
                                codeEditor: {
                                    extensions: [autocompletion()],
                                    extensionsKeymap: [completionKeymap],
                                },
                            }}
                        />
                    </CollapsibleItem>
                </Box>

                <Box px="1.2rem">
                    <CollapsibleItem
                        withoutMarginBottom
                        title="Javascript find() function"
                    >
                        <TaskTitle taskTitle="Lets try map() function" />
                        {/* <InfoPanel text="this is a text" /> */}
                        <TaskDescription>
                            Its time to learn map function. Map function is
                            common javascript function used quite often. Keep
                            focus on side effects which can happened as - what
                            happened is you fetched data failed, why is
                            important key and what can be used for key or how
                            output looks like.
                        </TaskDescription>

                        <TaskDescription>
                            How you handle data if you data will be object and
                            no array?
                        </TaskDescription>
                        <ResourcesLink href="https://medium.com/analytics-vidhya/learn-and-understand-javascripts-map-function-agbejule-kehinde-favour-ac91e3f61fff" />

                        <AssignmentList data={assignmentMapFunctionData} />

                        <Sandpack
                            template="react"
                            theme="dark"
                            files={{
                                "/App.js": {
                                    code: code,
                                    active: true,
                                },
                                "/data.js": {
                                    code: dataCode,
                                },
                            }}
                            options={{
                                recompileMode: "immediate",
                                showNavigator: false, // this will show a top navigator bar instead of the refresh button
                                showTabs: true, // you can toggle the tabs on/off manually
                                showLineNumbers: true, // this is off by default, but you can show line numbers for the editor
                                wrapContent: true, // also off by default, this wraps the code instead of creating horizontal overflow
                                editorWidthPercentage: 60, // by default the split is 50/50 between the editor and the preview
                                codeEditor: {
                                    extensions: [autocompletion()],
                                    extensionsKeymap: [completionKeymap],
                                },
                            }}
                        />
                    </CollapsibleItem>
                </Box>

                <Box px="1.2rem">
                    <CollapsibleItem
                        withoutMarginBottom
                        title="Javascript filter() function"
                    >
                        <TaskTitle taskTitle="Lets try map() function" />
                        {/* <InfoPanel text="this is a text" /> */}
                        <TaskDescription>
                            Its time to learn map function. Map function is
                            common javascript function used quite often. Keep
                            focus on side effects which can happened as - what
                            happened is you fetched data failed, why is
                            important key and what can be used for key or how
                            output looks like.
                        </TaskDescription>

                        <TaskDescription>
                            How you handle data if you data will be object and
                            no array?
                        </TaskDescription>
                        <ResourcesLink href="https://medium.com/analytics-vidhya/learn-and-understand-javascripts-map-function-agbejule-kehinde-favour-ac91e3f61fff" />

                        <AssignmentList data={assignmentMapFunctionData} />

                        <Sandpack
                            template="react"
                            theme="dark"
                            files={{
                                "/App.js": {
                                    code: code,
                                    active: true,
                                },
                                "/data.js": {
                                    code: dataCode,
                                },
                            }}
                            options={{
                                recompileMode: "immediate",
                                showNavigator: false, // this will show a top navigator bar instead of the refresh button
                                showTabs: true, // you can toggle the tabs on/off manually
                                showLineNumbers: true, // this is off by default, but you can show line numbers for the editor
                                wrapContent: true, // also off by default, this wraps the code instead of creating horizontal overflow
                                editorWidthPercentage: 60, // by default the split is 50/50 between the editor and the preview
                                codeEditor: {
                                    extensions: [autocompletion()],
                                    extensionsKeymap: [completionKeymap],
                                },
                            }}
                        />
                    </CollapsibleItem>
                </Box>

                <Box px="1.2rem">
                    <CollapsibleItem
                        withoutMarginBottom
                        title="Javascript reduce() function"
                    >
                        <TaskTitle taskTitle="Lets try map() function" />
                        {/* <InfoPanel text="this is a text" /> */}
                        <TaskDescription>
                            Its time to learn map function. Map function is
                            common javascript function used quite often. Keep
                            focus on side effects which can happened as - what
                            happened is you fetched data failed, why is
                            important key and what can be used for key or how
                            output looks like.
                        </TaskDescription>

                        <TaskDescription>
                            How you handle data if you data will be object and
                            no array?
                        </TaskDescription>
                        <ResourcesLink href="https://medium.com/analytics-vidhya/learn-and-understand-javascripts-map-function-agbejule-kehinde-favour-ac91e3f61fff" />

                        <AssignmentList data={assignmentMapFunctionData} />

                        <Sandpack
                            template="react"
                            theme="dark"
                            files={{
                                "/App.js": {
                                    code: code,
                                    active: true,
                                },
                                "/data.js": {
                                    code: dataCode,
                                },
                            }}
                            options={{
                                recompileMode: "immediate",
                                showNavigator: false, // this will show a top navigator bar instead of the refresh button
                                showTabs: true, // you can toggle the tabs on/off manually
                                showLineNumbers: true, // this is off by default, but you can show line numbers for the editor
                                wrapContent: true, // also off by default, this wraps the code instead of creating horizontal overflow
                                editorWidthPercentage: 60, // by default the split is 50/50 between the editor and the preview
                                codeEditor: {
                                    extensions: [autocompletion()],
                                    extensionsKeymap: [completionKeymap],
                                },
                            }}
                        />
                    </CollapsibleItem>
                </Box>
            </Box>
        </PageWrapper>
    );
};

export default Task;
