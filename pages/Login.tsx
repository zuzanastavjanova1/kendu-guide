import React, { useEffect, useState } from "react";
import { Button, Center, Stack, Text } from "@chakra-ui/react";
import { getProviders, signIn } from "next-auth/react";
import { NextPage } from "next";
import LoginContainerWrapper from "../src/components/Login/LoginContainerWrapper";
interface Provider {
    readonly callbackUrl: string;
    readonly id: string;
    readonly name: string;
    readonly signinUrl: string;
    readonly type: string;
}

const Login: NextPage = () => {
    const [providers, setProviders] = useState<Array<Provider>>([]);

    useEffect(() => {
        const fetchData = async () => {
            return await getProviders();
        };

        fetchData().then((data) => {
            const providers = data ? Object.values(data) : [];

            setProviders(providers);
        });
    }, []);

    if (!providers) {
        return (
            <LoginContainerWrapper
                mainWelcomeText="Ups, Houston, we have problem :("
                description="Time for ice cream"
            >
                <Center>
                    <Text>
                        We apologize, but shit happens. Fix is in progress.
                    </Text>
                </Center>
            </LoginContainerWrapper>
        );
    }

    return (
        <>
            <LoginContainerWrapper
                mainWelcomeText="Hello folks, Welcome!"
                description="Lets sign in and see what happens"
            >
                <Stack spacing="4" mt="8">
                    <Center>
                        {providers.map((provider) => (
                            <div key={provider.name}>
                                <Button
                                    bg="brand.yellow"
                                    size="md"
                                    type="submit"
                                    onClick={async () => await signIn("google")}
                                >
                                    Sign in with {provider.name}
                                </Button>
                            </div>
                        ))}
                    </Center>
                </Stack>
            </LoginContainerWrapper>
        </>
    );
};

export default Login;
