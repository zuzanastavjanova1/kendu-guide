import React, { FC } from "react";
import { PageWrapper } from "../src/components/PageWrapper";

// TODO - create normal nad custom error page wiht handle status codes

const ErrorPage: FC<{ statusCode: number }> = ({ statusCode }) => {
    return (
        <PageWrapper>
            {statusCode
                ? `An error ${statusCode} occurred on server`
                : "An error occurred on client"}
        </PageWrapper>
    );
};

export default ErrorPage;
