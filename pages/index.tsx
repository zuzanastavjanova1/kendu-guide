import React from "react";
import {
    Text,
    Stack,
    Heading,
    Box,
    Center,
    ListItem,
    ListIcon,
    List,
} from "@chakra-ui/react";
import { necessaryThings, topicsDivision } from "../src/data/aboutPageData";
import { PageWrapper } from "../src/components/PageWrapper";
import { useSession } from "next-auth/react";
import { Dots } from "../src/components/Navigation/Dots";
import { HomeBlock } from "../src/components/Reusable";

const Index = () => {
    const { data: session } = useSession();

    return (
        <PageWrapper>
            <Stack spacing="4" p="2rem">
                <Box
                    bg="brand.gradient"
                    minH={40}
                    p="12"
                    borderRadius={4}
                    color="white"
                >
                    <Heading as="h3" size="md" fontWeight="bold">
                        Hello {session.user.name}
                    </Heading>
                    <Text fontSize="sm" mt="4" fontWeight="bold">
                        We glad you join us and we are looking for your journey!
                    </Text>
                </Box>

                <Text fontSize="sm" px="12" pb="2">
                    This guide will help juniors find work in IT and give them
                    boundaries in the knowledge they need to know as juniors
                    developers. And also will help companies to find juniors who
                    will be able join their team with pre-approaved knowledge.
                </Text>
                <Center fontSize="sm" pb="6">
                    Do you miss something in this guide? Lets us know.
                </Center>
                <Center>
                    <Dots />
                </Center>

                <HomeBlock title="Motivation">
                    Are you confused about what to learn and how to learn? Where
                    find the best resources, what is important and what isnt?
                    There are too many resources, tutorials and courses are
                    available on the internet, But many of them are not useful
                    because of too much content. We are going to show way
                    throught resources in you path to became a a developer. Its
                    not neccesary know everythink in your level, is important to
                    know how you can gain skills and be orientet how to solve
                    problems a know principles. Here I have created a roadmap,
                    select articles, courses and video channels to becoming a
                    front-end developer or coder. Also you can find important
                    topics and free resources so you can start building your own
                    projects and get a job.
                </HomeBlock>

                <HomeBlock
                    title="What you need for your journey?"
                    badgeText="important"
                    description="  Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                    Etiam quis quam. Ut enim ad minim veniam, quis nostrud
                    exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Integer in sapien."
                >
                    <List spacing={3} pl="6" pb="6">
                        {necessaryThings.map((item) => (
                            <ListItem fontSize="sm" key={item.id}>
                                <ListIcon as={item.icon} color="grey" />
                                {item.text}
                            </ListItem>
                        ))}
                    </List>
                </HomeBlock>

                <HomeBlock
                    title="What you DONT need for your journey?"
                    badgeText="the most importan"
                >
                    University
                </HomeBlock>

                <HomeBlock title="  What you can expect from this guide?">
                    <Text fontSize="sm" pb="2">
                        The product will guide you through topics that a junior
                        developer should know and it is great to know them as a
                        developer at the start of a career.
                    </Text>
                    <Text fontSize="sm" pb="4">
                        Content will be separate to several group depend on
                        topic course. Each group should has got specific part:
                    </Text>

                    <List spacing={3} pl="6" pb="6">
                        {topicsDivision.map((item) => (
                            <ListItem fontSize="sm" key={item.id} pb="4">
                                <ListIcon as={item.icon} color="grey" />
                                {item.title}
                                <Text mt="2" ml="2">
                                    {item.description}
                                </Text>
                            </ListItem>
                        ))}
                    </List>
                </HomeBlock>
                <HomeBlock title="Watch your progress" badgeText="important">
                    You can see your progress in progress bar in right side bar
                    infront your yeys all the time. Each part has got several
                    parts and its possible to fill your topics as you want or if
                    you add checkmark for all resources from this topic, the
                    progress bar move you in your progress. Also you can see
                    your progress in your profile page, where you can find also
                    saved resources.
                </HomeBlock>
            </Stack>
        </PageWrapper>
    );
};

export default Index;
