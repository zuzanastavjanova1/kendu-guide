import React from "react";
import { NextPage } from "next";
import { PageWrapper } from "../../src/components/PageWrapper";
import TaskContent from "../../src/components/Tasks/TaskContent";

const Tasks: NextPage = () => {
    return (
        <PageWrapper>
            <TaskContent />
        </PageWrapper>
    );
};

export default Tasks;
