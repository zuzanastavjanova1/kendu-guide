import React from "react";
import { NextPage } from "next";
import { SimpleGrid, Box } from "@chakra-ui/react";
import { PageWrapper } from "../../src/components/PageWrapper";
import Link from "next/link";

const Projects: NextPage = () => {
    return (
        <PageWrapper>
            project
            <Box p="3.2rem">
                <SimpleGrid
                    mt="1.6rem"
                    columns={[1, 2]}
                    spacing={{ base: 2, lg: 4 }}
                >
                    <Link href="/project/[id]" as={`/project/1`}>
                        <Box bg="yellow.100" h="10rem">
                            HtmL and CSS project
                        </Box>
                    </Link>
                    <Link href="/project/[id]" as={`/project/2`}>
                        <Box bg="yellow.100" h="10rem">
                            React project
                        </Box>
                    </Link>
                </SimpleGrid>
            </Box>
        </PageWrapper>
    );
};

export default Projects;
