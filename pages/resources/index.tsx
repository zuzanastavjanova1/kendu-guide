import React from "react";
import Link from "next/link";
import { PageWrapper } from "../../src/components/PageWrapper";
import { useSections } from "../../src/services/useSections";
import { ListItem, UnorderedList } from "@chakra-ui/react";

// TODO: show just basic list of resources sections  - need to be changed

const Resources = () => {
    const sectionsData = useSections();
    return (
        <PageWrapper>
            <UnorderedList>
                {sectionsData?.map((sectionData) => (
                    <ListItem key={sectionData.name}>
                        <Link
                            href="/resources/[slug]"
                            as={`/resources/${sectionData.link}`}
                        >
                            <a> {sectionData.name}</a>
                        </Link>
                    </ListItem>
                ))}
            </UnorderedList>
        </PageWrapper>
    );
};

export default Resources;
