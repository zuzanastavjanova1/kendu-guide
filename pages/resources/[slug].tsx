import React, { FC } from "react";
import { getSession } from "next-auth/react";
import { useRouter } from "next/router";
import { Articles } from "../../src/components/Articles";
import { ContainerWrapper } from "../../src/components/ContainerWrapper";
import { KeyWords } from "../../src/components/Keywords";
import PageSection from "../../src/components/PageSection";
import { PageWrapper } from "../../src/components/PageWrapper";
import { Questions } from "../../src/components/Questions";
import { getMainInfo } from "../../src/components/Resources/getMainInfo";
import { useAuth } from "../../context/AuthUserContext";
import { ResourcesLevel } from "../../src/components/Resources/constants";

const SpecificResources: FC = () => {
    const router = useRouter();
    const { slug } = router.query;
    const { userId } = useAuth();
    const sectionFromSlug = slug?.toString();

    return (
        <PageWrapper>
            <ContainerWrapper>{getMainInfo(sectionFromSlug)}</ContainerWrapper>
            <PageSection
                section={sectionFromSlug}
                title="basic part"
                scrollTo={ResourcesLevel.BASIC}
            >
                {/* TODO: content will be different - depend on others components and features, so no nested more this component - need to be changed */}
                <KeyWords
                    section={sectionFromSlug}
                    filteredSection={sectionFromSlug}
                    sectionLevel={ResourcesLevel.BASIC}
                    userId={userId}
                />
                <Questions
                    section={sectionFromSlug}
                    filteredSection={sectionFromSlug}
                    sectionLevel={ResourcesLevel.BASIC}
                    userId={userId}
                />

                <Articles
                    section={sectionFromSlug}
                    filteredSection={sectionFromSlug}
                    sectionLevel={ResourcesLevel.BASIC}
                    userId={userId}
                />
            </PageSection>
            <PageSection
                section={sectionFromSlug}
                title="advance part"
                scrollTo={ResourcesLevel.ADVANCE}
            >
                <KeyWords
                    section={sectionFromSlug}
                    filteredSection={sectionFromSlug}
                    sectionLevel={ResourcesLevel.ADVANCE}
                    userId={userId}
                />
                <Questions
                    section={sectionFromSlug}
                    filteredSection={sectionFromSlug}
                    sectionLevel={ResourcesLevel.ADVANCE}
                    userId={userId}
                />
                <Articles
                    section={sectionFromSlug}
                    filteredSection={sectionFromSlug}
                    sectionLevel={ResourcesLevel.ADVANCE}
                    userId={userId}
                />
            </PageSection>
            <PageSection
                section={sectionFromSlug}
                title="pro part"
                scrollTo={ResourcesLevel.PRO}
            >
                <KeyWords
                    section={sectionFromSlug}
                    filteredSection={sectionFromSlug}
                    sectionLevel={ResourcesLevel.PRO}
                    userId={userId}
                />
                <Questions
                    section={sectionFromSlug}
                    filteredSection={sectionFromSlug}
                    sectionLevel={ResourcesLevel.PRO}
                    userId={userId}
                />

                <Articles
                    section={sectionFromSlug}
                    filteredSection={sectionFromSlug}
                    sectionLevel={ResourcesLevel.PRO}
                    userId={userId}
                />
            </PageSection>
        </PageWrapper>
    );
};

export default SpecificResources;

export async function getServerSideProps(context) {
    const session = await getSession(context);

    return {
        props: {
            session,
        },
    };
}
