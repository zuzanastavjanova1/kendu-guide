import { Box } from "@chakra-ui/react";
import { NextPage } from "next";
import { useSession } from "next-auth/react";
import React from "react";
import { PageWrapper } from "../src/components/PageWrapper";
import PersonalInformation from "../src/components/Profile/PersonalInformation";
import { SavedQuestion } from "../src/components/Profile/SavedQuestion";
import useUsersSavedIds from "../src/services/useUsersSavedIds";

const Profile: NextPage = () => {
    const { data: session } = useSession();
    const data = useUsersSavedIds(session.user.id);

    return (
        <PageWrapper>
            <Box p="20px">
                <PersonalInformation session={session} />
                <SavedQuestion userQuestionsIds={data?.questions} />
            </Box>
        </PageWrapper>
    );
};

export default Profile;
