import React from "react";
import { NextPage } from "next";
import {
    ProjectContainer,
    ProjectTaskItem,
} from "../../src/components/Projects";

const Project: NextPage = () => {
    return (
        <ProjectContainer
            projectName="Project name"
            motivation="Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                    Suspendisse sagittis ultrices augue. Donec quis nibh at
                    felis congue commodo. Aliquam id dolor. Morbi scelerisque
                    luctus velit. Etiam sapien elit, consequat eget, tristique
                    non, venenatis quis, ante. Etiam bibendum elit eget erat."
        >
            <ProjectTaskItem title="Setup page" content="content" />
            <ProjectTaskItem
                title="Create header and footer"
                content="content"
            />
        </ProjectContainer>
    );
};

export default Project;
