import { createContext, useContext } from "react";
import React from "react";

// TODO need to check the types ... dont like it
interface UserContextType {
    readonly authUser?: null | any;
    readonly userId: null | string;
    readonly userKeywords?: null | string[];
    readonly userQuestions?: null | string[];
    readonly userTopics?: null | string[];
    readonly userVideos?: null | string[];
    readonly userArticles?: null | string[];
    readonly userApis?: null | string[];
    readonly userGoal?: null | string;
}

const AuthUserContext = createContext({
    authUser: null,
    userId: null,
    userKeywords: null,
    userQuestions: null,
    userTopics: null,
    userVideos: null,
    userArticles: null,
    userApis: null,
    userGoal: null,
} as UserContextType);

export const AuthUserProvider: React.FC<UserContextType> = ({
    children,
    authUser,
    userId,
    userKeywords,
    userQuestions,
    userTopics,
    userVideos,
    userArticles,
    userApis,
    userGoal,
}) => {
    return (
        <AuthUserContext.Provider
            value={{
                authUser,
                userId,
                userKeywords,
                userQuestions,
                userTopics,
                userVideos,
                userArticles,
                userApis,
                userGoal,
            }}
        >
            {children}
        </AuthUserContext.Provider>
    );
};

export const useAuth = (): UserContextType => {
    return useContext(AuthUserContext);
};
